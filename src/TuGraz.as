package
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.HTTPStatusEvent;
	import flash.events.IOErrorEvent;
	import flash.events.SecurityErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import events.ReadyEvent;
	import objects.Strings;
	import starling.text.TextField;
	import flash.utils.getTimer;

	
	
	public class TuGraz extends Sprite
	{
		private static var _learner: String;
		private static var _process: String;
		private static var _nextTask: String;
		private static var _competences: Array;
		private static var textField: TextField;
		private static var path: String;				// Path della richiesta al service.
		private static var body: String;				// Body della richiesta al service.
		public static var response: String;				// Risposta del service;
		private static var split: Array;
		private static var nProcesses: int;
		private static var _nCompetences: int;
		private static var loader: URLLoader;
		private static var request: URLRequest;
		private static var theRequest: String;
		private static var method: String;
		public static var go: Boolean;
		private static var _startTime : int;
		
		
		public function TuGraz() 
		{
			super();
			
			_nCompetences = 1;
		}
		
		public static function get nCompetences():int
		{
			return _nCompetences;
		}
		
		public static function get learner():String
		{
			return _learner;
		}
		
		public static function get process():String
		{
			return _process;
		}
		
		public static function get nextTask():String
		{			
			return _nextTask;
		}
		
		public static function get competences():Array
		{
			return _competences;
		}
		
		public static function setLearner(): void
		{
			_learner = "user" + nProcesses.toString();
		}
		
		public function interact(aRequest: String): void
		{
			theRequest = aRequest;
			
			loader = new URLLoader();
			loader.dataFormat = URLLoaderDataFormat.TEXT;
			loader.addEventListener(Event.COMPLETE, loaderCompleteHandler);
			loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, httpStatusHandler);
			loader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
			loader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
			
			switch(theRequest)
			{
				case Strings.INIT:												// OK
				{					
					_learner = "learner";
					method = Strings.POST;
					path = Strings.INIT_PATH;
					body = 'data=<message type="request" clientid="gala" learnerid="' + _learner + '" domainmodelid="gala-thejourney"></message>';
					
					break;
				}
					
				case Strings.STOP:												//	OK
				{
					method = Strings.GET;
					path = Strings.STOP_PATH + _process;
					
					break;
				}
					
				case Strings.DELETE:											//	OK
				{
					method = Strings.GET;
					path = Strings.DELETE_PATH + _process; 
					
					break;
				}
					
				case Strings.NEXT_TASK:											// OK
				{
					method = Strings.GET;
					path = Strings.NEXT_TASK_PATH + _process;
					
					break;
				}
					
				case Strings.COMMIT:											//	OK
				{
					method = Strings.POST;
					path = Strings.COMMIT_PATH;
					body = 'data=<message type="request" learningprocessid="' + _process +'"><problem id="' + _nextTask + '" solved="';
					body += GameInfo.commit + '"/></message>';
					
					break;
				}
					
				case Strings.COMPETENCES:										// OK
				{
					method = Strings.GET;
					path = Strings.COMPETENCES_PATH + _process;
					
					break;
				}
					
				case Strings.PROC:
				{
					method = Strings.GET;
					path = Strings.PROC_PATH;
					
					break;
				}
			}
			
			request = new URLRequest(path);
			
			if(method == Strings.POST)
			{
				//request.method = URLRequestMethod.POST;
				request.method = "POST";
				request.contentType = "text/plain";
				request.data = body;
			}
			trace("Connecting to the service... Making request '" + aRequest + "' ");
			response = "connecting...";
			_startTime = getTimer();
			loader.load(request);
			
		}
		
		protected function ioErrorHandler(event: IOErrorEvent): void
		{
			response = response + "ioErrorHandler: " + event.text;
		}
		
		protected function securityErrorHandler(event: SecurityErrorEvent): void
		{
			response = "securityErrorHandler:" + event.text;
		}
		
		protected function httpStatusHandler(event:HTTPStatusEvent): void
		{
			response = response + "httpStatusHandler:" + event.status;
		}
		
		protected function loaderCompleteHandler(event: Event): void
		{
			response += event.target.data;
			var _stopTime:int = getTimer();
			var duration:int = _stopTime - _startTime;
            trace("Duration of the request: " + duration + " ms");
			
			switch(theRequest)
			{
				case Strings.INIT:
				{
					split = response.split('"');
					_process = split[3];
					
					Game.database.dispatchEvent(new ReadyEvent(ReadyEvent.SVC_INIT, true));
					
					break;
				}
					
				case Strings.NEXT_TASK:
				{
					split = response.split('"');
					_nextTask = split[3];
					
					setScreen();
					
					if(!GameInfo.leftAns && !GameInfo.rightAns)
						
						Game.database.dispatchEvent(new ReadyEvent(ReadyEvent.SVC_NEXT1, true));
						
					else
						
						Game.database.dispatchEvent(new ReadyEvent(ReadyEvent.SVC_NEXT2, true));
					
					break;
				}
					
				case Strings.COMPETENCES:
				{
					var i: int;
					var l: int;
					_competences = new Array();
					
					split = response.split('"');
					l = split.length - 2;
					
					for(i = 3; i <= l; i += 2)
					{
						_competences[i - 3] = split[i];
					}
					
					_nCompetences = _competences.length;
					
					Game.database.dispatchEvent(new ReadyEvent(ReadyEvent.SVC_COMP, true));
					
					break;
				}
					
				case Strings.COMMIT:
				{
					Game.database.dispatchEvent(new ReadyEvent(ReadyEvent.SVC_COMMITTED, true));
					
					break;
				}
					
				case Strings.STOP:
				{
					Game.database.dispatchEvent(new ReadyEvent(ReadyEvent.SVC_STOP, true));
					
					break;
				}
					
				case Strings.DELETE:
				{
					Game.database.dispatchEvent(new ReadyEvent(ReadyEvent.SVC_DELETE, true));
					
					break;
				}
			}
			
			go = true;
		}
		
		public function setScreen(): void
		{			
			switch(_nextTask)
			{
				case "task06": case "task07": case "task08": case "task10": case "task11": case "task12": case "task21": case "task22":
				case "task23": case "task24":
				{
					GameInfo.screen = Strings.MOUNTAINS;
					
					break;
				}
					
				case "task09": case "task13": case "task14": case "task16": case "task18": case "task19": case "task25":
				{
					GameInfo.screen = Strings.LAKE;
					
					break;
				}
					
				case "task01": case "task02": case "task03": case "task04": case "task05": case "task15": case "task17": case "task20":
				{
					GameInfo.screen = Strings.RIVER;
					
					break;
				}
			}
		}
		
	}
}