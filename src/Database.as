package
{
	import flash.data.SQLConnection;
	import flash.data.SQLResult;
	import flash.data.SQLStatement;
	import flash.events.SQLErrorEvent;
	import flash.events.SQLEvent;
	import flash.filesystem.File;
	import events.ReadyEvent;
	import starling.display.Sprite;
	
	
	public class Database extends Sprite
	{
		private static var conn: SQLConnection;
		private static var folder: File;
		private static var dbFile: File;
		private static var query: SQLStatement;
		public static var query_text: String;
		private static var result: SQLResult;
		private static var row: Object;
		private static var _params: Array;
		public static var status: String;
		
		
		public function Database()
		{
			super();
		}
		
		public static function get params():Array
		{
			return _params;
		}
		
		public function ConnectDb(fileName: String): void
		{
			conn = new SQLConnection();
			conn.addEventListener(SQLEvent.OPEN, openHandler);
			conn.addEventListener(SQLErrorEvent.ERROR, errorHandler);
			
			// Creo il file nell'application storage directory 
			folder = File.applicationStorageDirectory; 
			dbFile = folder.resolvePath(fileName);
			
			// Se il database è già esistente, apro la connessione, altrimenti creo il database ed effettuo la connessione. 
			conn.openAsync(dbFile);
		}
		
		public function CreateTable(table: String): void
		{
			query = new SQLStatement();
			query.sqlConnection = conn;
			
			if(table == "tasks")
			
				query_text = "CREATE TABLE IF NOT EXISTS Tasks(game TEXT, taskId TEXT PRIMARY KEY, text TEXT, type TEXT)";
			
			else if(table == "competences")
				
				query_text = "CREATE TABLE IF NOT EXISTS Competences(compId TEXT PRIMARY KEY, p NUMERIC)";
			
			else
				
				query_text = "CREATE TABLE IF NOT EXISTS CompTask(compId TEXT PRIMARY KEY, taskId TEXT PRIMARY KEY)";
			
			query.text = query_text;
			
			query.addEventListener(SQLEvent.RESULT, createResult);
			query.addEventListener(SQLErrorEvent.ERROR, errorHandler);
			
			query.execute();
		}
		
		public function InsertRow(values: String): void
		{
			query = new SQLStatement();
			query.sqlConnection = conn;
			
			query_text = "INSERT INTO Tasks(game, taskId, text, type) VALUES " +  values;
			query.text = query_text;
			
			query.addEventListener(SQLEvent.RESULT, insertResult);
			query.addEventListener(SQLErrorEvent.ERROR, errorHandler);
			
			query.execute();
		}
		
		public function InsertRow2(values: String): void
		{
			query = new SQLStatement();
			query.sqlConnection = conn;
			
			query_text = "INSERT INTO Competences(compId TEXT PRIMARY KEY, p NUMERIC) VALUES " +  values;
			query.text = query_text;
			
			query.addEventListener(SQLEvent.RESULT, insertResult);
			query.addEventListener(SQLErrorEvent.ERROR, errorHandler);
			
			query.execute();
		}
		
		public function Query(task: String): void
		{
			status = "NIENTE";
			query = new SQLStatement();
			query.sqlConnection = conn;
			query.text = "SELECT * FROM Tasks WHERE taskId = '" + task + "'";
			
			status = "QUERY";
			
			query.addEventListener(SQLEvent.RESULT, queryResult);
			query.addEventListener(SQLErrorEvent.ERROR, errorHandler);
			
			status = "CHIEDO"
			
			query.execute();
			
			status = "CHIESTO";
		}
		
		public function Update(): void
		{
			var text: String = "Going on this road, you  will come across a river. There is a bridge over the river that you can cross,  unless it is closed for maintenance (p = p1). In addition, you can cross the bridge if it is sunny (p = p2)";
			text += "or if it is foggy but with no precipitation (p = p3). Anyother weather conditions do not affect thetravelling condition of this path. What is theprobability to be able to cross the river?";
			status = "NIENTE";
			query = new SQLStatement();
			query.sqlConnection = conn;
			query.text = "UPDATE Tasks SET text = '" + text + "' WHERE taskId = 'task20'";
			
			status = "QUERY";
			
			query.addEventListener(SQLEvent.RESULT, queryResult);
			query.addEventListener(SQLErrorEvent.ERROR, errorHandler);
			
			status = "CHIEDO"
			
			query.execute();
			
			status = "CHIESTO";
		}
		
		protected function queryResult(event: SQLEvent): void
		{
			var numResults:int;
			
			result = query.getResult(); 
			row = result.data[0];
			_params = new Array(row.text);
			status = "Query avvenuta con successo!! " + row.text;
			
			this.dispatchEvent(new ReadyEvent(ReadyEvent.DB_RESPONSE, true));
		}
		
		protected function insertResult(event:SQLEvent):void
		{
			status = "Inserimento avvenuto con successo!!";
			this.dispatchEvent(new ReadyEvent(ReadyEvent.ROW_INSERTED, true));
		}
		
		protected function createResult(event: SQLEvent): void
		{
			status = "Tabella creata correttamente!!";
			trace(query_text);
			this.dispatchEvent(new ReadyEvent(ReadyEvent.TABLE_CREATED, true));
		}
		
		protected function openHandler(event: SQLEvent): void
		{
			status = "Connessione avvenuta correttamente!!";
			this.dispatchEvent(new ReadyEvent(ReadyEvent.DB_CONNECTED, true));
		}
		
		protected function errorHandler(event: SQLErrorEvent): void
		{
			status = "Error message: " + event.error.message + ". Details: " + event.error.details;
			this.dispatchEvent(new ReadyEvent(ReadyEvent.ROW_INSERTED, true));
		}		
	}
}