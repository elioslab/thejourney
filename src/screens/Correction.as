package screens
{
	import events.NavigationEvent;
	import objects.Colors;
	import objects.Strings;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	
	public class Correction extends Sprite
	{
		private var bg: Image;
		private var image1: Image;
		private var image2: Image;
		private var image3: Image;
		private var left: Button;
		private var right: Button;
		private var back: Button;
		private var text: String;
		private var textField: TextField;
		private var txtField1: TextField;
		private var txtField2: TextField;
		private var txtField3: TextField;
		private var txtField4: TextField;
		private var color: uint;
		
		
		public function Correction()
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			bg = new Image(Assets.getTexture("bgNotes"));
			this.addChild(bg);
			
			trace("\nTRACE");
			trace(GameInfo.lCorrect.toString() + GameInfo.lCorrect.toString()); 
			
			if(!GameInfo.lCorrect && GameInfo.rCorrect)
			{	
				trace("\n SX SBAGLIATA, DX GIUSTA");
				correctTask(GameInfo.lTask);
			}	
			else if(GameInfo.lCorrect && !GameInfo.rCorrect)
			{	
				trace("\n SX GIUSTA, DX SBAGLIATA");
				correctTask(GameInfo.rTask);
			}
			
			else
			{
				trace("\n ENTRAMBE SBAGLIATE");
				correctTask(GameInfo.lTask);
				right = new Button(Assets.getTexture("rArrow"));
				right.width = 120;
				right.height = 80;
				right.x = 670;
				right.y = 510;
				this.addChild(right);
			}
			
			back = new Button(Assets.getAtlas().getTexture("backButton"));
			back.x = 20;
			back.y = 430;
			this.addChild(back);
			
			this.addEventListener(Event.TRIGGERED, onClick);
		}		
		
		private function onClick(event: Event):void
		{
			var buttonClicked: Button = event.target as Button;
			
			if((buttonClicked as Button) == left)
			{
				this.removeChildren();
				this.addChild(bg);
				this.correctTask(GameInfo.lTask);
				this.addChild(right);
				this.addChild(back);
			}
				
			else if((buttonClicked as Button) == right)
			{
				this.removeChildren();
				this.addChild(bg);
				this.correctTask(GameInfo.rTask);
				left = new Button(Assets.getTexture("lArrow"));
				left.width = 120;
				left.height = 80;
				left.x = 670;
				left.y = 510;
				this.addChild(left);
				this.addChild(back);
			}
				
			else if((buttonClicked as Button) == back)
			{
				var textField: TextField = new TextField(400, 400, "BACK", "HoboStd", 50, Colors.PEN);
				textField.x = 0;
				textField.y = 0;
				this.addChild(textField);
				
				//this.visible = false;
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.BACK_CORRECTION}, true));
			}
			
		}
		
		private function correctTask(task: String):void
		{
			loadImage(task);
			setTxt(task);
			
			if(task == "task11" || task == "task12" || task == "task13" || task == "task14" || task == "task15" || task == "task17" || task == "task20"
				|| task == "task25")
				
				loadOtherImages(task);
			
			setExplaination(task);
			
			text = "This is the rapresentation of the events space.";
			textField = new TextField(image1.width, 100, text, "HoboStd", 20, color);
			textField.x = image1.x;
			textField.y = image1.y + image1.height + 20;
			textField.hAlign = HAlign.CENTER;
			textField.vAlign = VAlign.TOP;
			this.addChild(textField);
		}
		
		private function loadOtherImages(task: String):void
		{
			switch(task)
			{
				case "task11": case "task12":
				{
					image2 = new Image(Assets.getTexture("task11"));
					image2.width = image1.width / 2;
					image2.height = image1.height / 2;
					image2.x = 500;
					image2.y = 380;
					this.addChild(image2);
					
					break;
				}
					
				case "task13":
				{
					image2 = new Image(Assets.getTexture("task13"));
					image2.width = image1.width / 2;
					image2.height = image1.height / 2;
					image2.x = 500;
					image2.y = 380;
					this.addChild(image2);
					
					break;
				}
					
				case "task14":
				{
					image2 = new Image(Assets.getTexture("task14"));
					image2.width = image1.width / 2;
					image2.height = image1.height / 2;
					image2.x = 500;
					image2.y = 380;
					this.addChild(image2);
					
					break;
				}
					
				case "task15":
				{
					image2 = new Image(Assets.getTexture("task15_1"));
					image2.width = image1.width / 2;
					image2.height = image1.height / 2;
					image2.x = 410;
					image2.y = 340;
					this.addChild(image2);
					image3 = new Image(Assets.getTexture("task15_2"));
					image3.width = image2.width;
					image3.height = image2.height;
					image3.x = image2.x + image2.width + 10;
					image3.y = image2.y;
					this.addChild(image3);
					
					break;
				}
					
				case "task17":
				{
					image2 = new Image(Assets.getTexture("task17_1"));
					image2.width = image1.width / 2;
					image2.height = image1.height / 2;
					image2.x = 410;
					image2.y = 340;
					this.addChild(image2);
					image3 = new Image(Assets.getTexture("task17_2"));
					image3.width = image2.width;
					image3.height = image2.height;
					image3.x = image2.x + image2.width + 10;
					image3.y = image2.y;
					this.addChild(image3);
					
					break;
				}
					
				case "task20":
				{
					image2 = new Image(Assets.getTexture("task20_1"));
					image2.width = image1.width / 2;
					image2.height = image1.height / 2;
					image2.x = 410;
					image2.y = 370;
					this.addChild(image2);
					image3 = new Image(Assets.getTexture("task20_2"));
					image3.width = image2.width;
					image3.height = image2.height;
					image3.x = image2.x + image2.width + 10;
					image3.y = image2.y;
					this.addChild(image3);
					
					break;
				}
					
				case "task25":
				{
					image2 = new Image(Assets.getTexture("task25_1"));
					image2.width = image1.width / 2;
					image2.height = image1.height / 2;
					image2.x = 410;
					image2.y = 340;
					this.addChild(image2);
					image3 = new Image(Assets.getTexture("task25_2"));
					image3.width = image2.width;
					image3.height = image2.height;
					image3.x = image2.x + image2.width + 10;
					image3.y = image2.y;
					this.addChild(image3);
					
					break;
				}
			}
		}
		
		private function setExplaination(task: String):void
		{
			switch(task)
			{
				case "task01": case "task03": case "task05":
				{
					text = "Between the n people who tried to take the path, only x managed. This means that the estimated probability";
					text += " to overcome the path is x / n, while the extimated probability not to overcome is (n - x) / n.";
					
					textField = new TextField(300, 500, text, "HoboStd", 20, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task02": case "task04":
				{
					text = "Between the n people who tried to take the path, x did not manage. This means that the estimated probability";
					text += " not to overcome the path is x / n, while the extimated probability to overcome is (n - x) / n.";
					
					textField = new TextField(300, 500, text, "HoboStd", 20, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task06": case "task07":
				{
					text = "This path is closed if it is snowing or if the temperature is higher than 25°C. Since the two events cannot happen ";
					text += "at the same moment, them are mutually exclusive events. In addiction, any other weather conditions do not affect the ";
					text += "travelling possibilities of the path, so the probability to overcome the path is 1 - (p_highT + p_snow), while the ";
					text += "probability not to overcome the path is p_highT + p_snow.";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task08":
				{
					text = "The path is closed because of snow on the road or cows eating grass. Since cows never eat grass when it is snowing, the two ";
					text += "events are mutually exclusive. In the year of 2011, so in 365 days, it snowed for n_snow days and there were cows blocking  ";
					text += "the way for n_cow days. Then, the probability not to overcome the path is (n_snow + n_cow) / n. In addiction, any other ";
					text += "weather conditions do not affect the travelling possibilities of the path, so the probability to overcome the path is ";
					text += "1 - ((n_snow + n_cow) / 365).";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task09":
				{
					text = "The path is travelable only if it is hot or if it is cold but sunny. The two events are mutually exclusive, so the probability";
					text += "to overcome the path is p_hot + p_cANDs.";
					
					textField = new TextField(300, 500, text, "HoboStd", 20, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task10":
				{
					text = "The path is closed because of high temperature or snow. If it is hot, it can not snow, so the events are mutually exclusive. In ";
					text += "n days, the temperature was high for n_hot days and it was snowy for n_snow days. Then, the extimated probability not to";
					text += "overcome the path is (n_hot + n_snow) / n. In addiction, any other weather conditions do not affect the travelling ";
					text += "possibilities of the path, so the probability to overcome the path is 1 - ((n_hot + n_snow) / n)).";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task11": case "task12":
				{
					text = "The road is closed if it is windy, if it is rainy or if it is windy and rainy. In order to calculate the probability that the ";
					text += "road is closed, you can not only add p_wind and p_rain, because, in this way, you would consider p_rANDw (the violet area in ";
					text += "the picture on the left) twice. So, the probability that the road is closed (the violet area in the picture below) is: ";
					text += "p_rain + p_wind – p_rANDw.";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task13":
				{
					text = "The boat does not leave in case of precipitations, in case of wind, or in case of both the events. In order to calculate the ";
					text += "probability that the boat does not leave, you can not only add p_wind and p_prec, because, in this way, you would consider ";
					text += "p_precANDw (the violet area in the picture on the left) twice. So, the probability that the boat does not leave (the violet ";
					text += "area in the picture below) is: p_rain + p_wind – p_precANDw.";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task14":
				{
					text = "The boat does not leave in case of precipitations, in case of fog, or in case of both the events. In order to calculate the ";
					text += "probability that the boat does not leave, you can not only add p_prec and p_fog, because, in this way, you would consider ";
					text += "p_pANDf (the violet area in the picture on the left) twice. So, the probability that the boat does not leave (the violet area ";
					text += "in the picture below) is: p_prec + p_fog – p_pANDf.";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task15":
				{
					text = "Or it is windy or it is not windy, so the situation regarding the wind is the one represented in the picture above. The bridge ";
					text += "is crossable if it is sunny and not windy (the violet part in the picture on the left). The probability that it is sunny or ";
					text += "windy (the violet part in the picture below) is p_sORnotw = p_sun + p_notwind – p_sANDnotw. Then, p_sANDnotw = p_sun + ";
					text += "p_notwind – p_sORnotw = p_sun + (1 - p_wind) – p_sORnotw.";
					
					textField = new TextField(300, 500, text, "HoboStd", 16, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					
					break;
				}
					
				case "task16":
				{
					text = "The fact that you find the boat and the fact that it works are independent events, so the probability to overcome the path";
					text += " is p_boat * p_func.";
					
					textField = new TextField(300, 500, text, "HoboStd", 20, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task17":
				{
					text = "The bridge could only be open or close. In the same way, there could be upkeep or not. The bridge state (open or close) and";
					text += "the bridge condition (upkeep or not upkeep) are independent events. Then, the probability to cross the river is ";
					text += "(1 - p_open) * (1 - p_uk)";
					
					textField = new TextField(300, 500, text, "HoboStd", 20, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task18":
				{
					text = "The boat presence does not influence the captain presence, so events are independent. Then, the probability to reach the other";
					text += "side of the lake is p_boat * p_cap.";
					
					textField = new TextField(300, 500, text, "HoboStd", 20, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task19":
				{
					text = "The probability to find enough people is always the same. This means that finding the boat and finding enough people are ";
					text += "independent events. Then, the probability to reach the other side of the lake is n_boat * p_enough / n.";
					
					textField = new TextField(300, 500, text, "HoboStd", 20, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task20":
				{
					text = "The bridge condition (upkeep or not upkeep) and the weather condition are independent events. Regarding the bridge condition, ";
					text += "there could be upkeep or not. There are not other options. Regarding the weather conditions, it can’t be out of the blue and";
					text += "foggy in the meantime, so the events are mutually exclusive. Then, the probability to cross the river is ";
					text += "(1 - p_uk) * (p_ootb + p_fANDnotp).";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task21": case "task22":
				{
					text = "The weather conditions and the presence of cows are not independent events, so, in order to calculate the probability that the";
					text += "path is closed (Pr(A,B) in the formula), you have to multiply the probability that it is snowing and the probability to find ";
					text += "cows eating grass given that it is snowy. Then, the probability that the path is not travellable is ";
					text += "1 - (cows|snow * p_snow).";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task23": case "task24":
				{
					text = "The weather conditions and the landslide are not independent events, so, in order to calculate the probability that the path ";
					text += "is closed (Pr(A,B) in the formula), you have to multiply the probability that it is snowing and the probability of landslide";
					text += "given that it is snowing. Then, the probability to overcome the path is 1 - (p_land|snow * p_snow).";
					
					textField = new TextField(300, 500, text, "HoboStd", 18, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
					
				case "task25":
				{
					text = "The boat presence and the fact that it can leave or not are independent events, then, the probability to reach the other ";
					text += "side is p_boat * p_canLeave. It can be sunny or not. There are not other options. The presence of enough tourists and ";
					text += "the weather conditions are not independent events, so, the probability to have the condition to leave is p_tou|sun * p_sun.";
					text += "To summarize, the probability to overcome the path is p_boat * (p_tou|sun * p_sun).";
					
					textField = new TextField(300, 500, text, "HoboStd", 16, Colors.PEN);
					textField.x = 415;
					textField.y = 40;
					
					break;
				}
			}
			
			textField.hAlign = HAlign.CENTER;
			textField.vAlign = VAlign.TOP;
			this.addChild(textField);
		}
		
		private function loadImage(task: String): void
		{
			switch(task)
			{
				case "task01": case "task02": case "task03": case "task04": case "task05":
				{
					image1 = new Image(Assets.getTexture("prompt1"));
					
					color = Colors.BLUE;
					
					break;
				}
					
				case "task06": case "task07": case "task08": case "task09": case "task10":
				{
					image1 = new Image(Assets.getTexture("prompt2"));
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task11": case "task12": case "task13": case "task14": case "task15":
				{
					image1 = new Image(Assets.getTexture("prompt3"));
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task16": case "task17": case "task18": case "task19": case "task20": case "task25":
				{
					image1 = new Image(Assets.getTexture("prompt5"));
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task21": case "task22": case "task23": case "task24": 
				{
					image1 = new Image(Assets.getTexture("prompt6"));
					
					color = Colors.PINK;
					
					break;
				}
			}
			
			image1.width = 318;
			image1.height = 230;
			image1.x = 55;
			image1.y = 100;
			this.addChild(image1);
		}
		
		private function setTxt(task: String):void
		{
			switch(task)
			{
				case "task01": case "task02": case "task03": case "task04": case "task05":
				{
					txtField1 = new TextField(100, image1.height, "x / n", "HoboStd", 25, Colors.PINK);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 2) + 30;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(125, image1.height, "( n - x) / n", "HoboStd", 25, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 2) - 145;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.BLUE;
					
					break;
				}
					
				case "task06": case "task07": case "task10":
				{
					txtField1 = new TextField(115, image1.height, "P(highT)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 88;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(115, image1.height, "P(snow)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 48;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task08":
				{
					txtField1 = new TextField(115, image1.height, "P(cow)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 88;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(snow)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 48;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task09":
				{
					txtField1 = new TextField(115, image1.height, "P(hot)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 88;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(cANDs)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 48;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task11": case "task12":
				{
					txtField1 = new TextField(115, image1.height, "P(wind)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 98;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(rain)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 58;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task13":
				{
					txtField1 = new TextField(115, image1.height, "P(prec)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 98;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(wind)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 58;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task14":
				{
					txtField1 = new TextField(115, image1.height, "P(prec)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 98;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(fog)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 58;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					
					break;
				}
					
				case "task15":
				{
					txtField1 = new TextField(115, image1.height, "P(sun)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 98;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(NOTw)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 58;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task16":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(boat)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(func)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task17":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(close)", "HoboStd", 18, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(NOTuk)", "HoboStd", 18, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task18":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(boat)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(cap)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task19":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(boat)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(tou)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task20":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(NOTuk)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(weat)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task21": case "task22":
				{
					txtField1 = new TextField(122, 119, "P(snow)", "HoboStd", 18, Colors.PINK);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + image1.width / 2 + 10;
					txtField1.y = image1.y + image1.height / 2;
					this.addChild(txtField1);
					
					txtField2 = new TextField(50, 119, "P(c|s)", "HoboStd", 10, Colors.VIOLET);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = 210;
					txtField2.y = image1.y + image1.height / 4 - 25;
					this.addChild(txtField2);
					
					txtField3 = new TextField(122, 119, "P(NOTsnow)", "HoboStd", 18, Colors.PINK);
					txtField3.hAlign = HAlign.CENTER;
					txtField3.vAlign = VAlign.CENTER;
					txtField3.x = 90;
					txtField3.y = image1.y + image1.height / 2;
					this.addChild(txtField3);
					
					txtField4 = new TextField(122, 119, "P(c|NOTs)", "HoboStd", 10, Colors.VIOLET);
					txtField4.hAlign = HAlign.CENTER;
					txtField4.vAlign = VAlign.CENTER;
					txtField4.x = 140;
					txtField4.y = image1.y + image1.height / 4 + 20;
					this.addChild(txtField4);
					
					color = Colors.PINK;
					
					break;
				}
					
				case "task23": case "task24":
				{
					txtField1 = new TextField(122, 119, "P(snow)", "HoboStd", 18, Colors.PINK);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + image1.width / 2 + 10;
					txtField1.y = image1.y + image1.height / 2;
					this.addChild(txtField1);
					
					txtField2 = new TextField(50, 119, "P(l|s)", "HoboStd", 10, Colors.VIOLET);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = 210;
					txtField2.y = image1.y + image1.height / 4 - 25;
					this.addChild(txtField2);
					
					txtField3 = new TextField(122, 119, "P(NOTsnow)", "HoboStd", 18, Colors.PINK);
					txtField3.hAlign = HAlign.CENTER;
					txtField3.vAlign = VAlign.CENTER;
					txtField3.x = 90;
					txtField3.y = image1.y + image1.height / 2;
					this.addChild(txtField3);
					
					txtField4 = new TextField(122, 119, "P(l|NOTs)", "HoboStd", 10, Colors.VIOLET);
					txtField4.hAlign = HAlign.CENTER;
					txtField4.vAlign = VAlign.CENTER;
					txtField4.x = 140;
					txtField4.y = image1.y + image1.height / 4 + 20;
					this.addChild(txtField4);
					
					color = Colors.PINK;
					
					break;
				}
					
				case "task25":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(boat)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(canLeave)", "HoboStd", 14, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
			}
			
			text = "This is the rapresentation of the events space.";
			textField = new TextField(image1.width, 100, text, "HoboStd", 20, color);
			textField.x = image1.x;
			textField.y = image1.y + image1.height + 20;
			textField.hAlign = HAlign.CENTER;
			textField.vAlign = VAlign.TOP;
			this.addChild(textField);
		}
	}
}