package screens
{
	import events.NavigationEvent;
	
	import objects.Colors;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import objects.Strings;
	
	
	public class About extends Sprite
	{
		private var background: Image;
		private var name1: Image;
		private var university: Image
		private var unige: Image;
		private var hsharma: Image;
		private var starling: Image;
		private var back: Button;
		private var text: TextField;
		private var html: HTML;
		
		public function About()
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			trace("About screen inizialized");
			
			background = new Image(Assets.getTexture("bgNotes"));
			this.addChild(background);
			
			text = new TextField(320, 100, "This project is made by", "HoboStd", 30, Colors.PINK_PEN);
			text.x = 400;
			text.y = 35;
			this.addChild(text);
			
			name1 = new Image(Assets.getTexture("name"));
			name1.x = 405;
			name1.y = 120;
			this.addChild(name1);
			
			text = new TextField(320, 140, "Department of Electrical, Electronic, Telecommunications Engineering and Naval Architecture.", "HoboStd", 20, Colors.PINK_PEN);
			text.x = 400;
			text.y = 270;
			this.addChild(text);
			
			text = new TextField(50, 50, ",", "HoboStd", 30, 0x003399);
			text.x = 695;
			text.y = 230;
			this.addChild(text);
			
			university = new Image(Assets.getTexture("university"));
			university.x = 65;
			university.y = 60;
			this.addChild(university);
			
			unige = new Image(Assets.getTexture("unige"));
			unige.x = 145;
			unige.y = 150;
			this.addChild(unige);
			
			hsharma = new Image(Assets.getTexture("hsharma"));
			hsharma.x = 125;
			hsharma.y = 330;
			this.addChild(hsharma);
			
			starling = new Image(Assets.getTexture("starling"));
			starling.x = 125;
			starling.y = 420;
			this.addChild(starling);
			
			back = new Button(Assets.getAtlas().getTexture("backButton"));
			back.x = 630;
			back.y = 410;
			this.addChild(back);
			
			this.addEventListener(Event.TRIGGERED, onClick);
		}
		
		private function onClick():void
		{
			html.removeHtml();
			this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: "back"}, true));
		}
		
		public function inizialize():void
		{
			this.visible = true;
			html = new HTML(Strings.ARTWORK_CREDITS, 420, 420);
			
			this.addEventListener(Event.ENTER_FRAME, animations);
		}
		
		private function animations():void
		{
			var date: Date = new Date();
			back.y = 410 + (Math.cos(date.getTime() * 0.002) * 10);
		}
	}
}