package screens
{
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.utils.Timer;
	
	import events.NavigationEvent;
	import events.ReadyEvent;
	
	import inner_class.InnerClass;
	
	import objects.Colors;
	import objects.Numbers;
	import objects.Strings;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	
	public class Notes extends Sprite
	{
		private var background: Image;
		private var cont:Button;
		private var help: Button;
		private var nextScreen: String;
		private var textField: starling.text.TextField;
		private var currentTask: String;
		private var task: Tasks;
		private var ynAnswer: String;				// Risposta corretta se il task è di tipo yes / no.
		private var answer: Number;					// Risposta corretta se il task è di tipo choose right option oppure enter value.
		private var aButton: Button;
		private var option1: Number;				// Valore della opzione 1 nel caso di task di tipo choose right option.
		private var o1Button: Button;
		private var option2: Number;				// Valore della opzione 2 nel caso di task di tipo choose right option.
		private var o2Button: Button;
		private var yn: Boolean;					// Se true, il task è di tipo yes / no.
		private var cro: Boolean;					// Se true, il task è di tipo choose right option.
		private var uAnswer: Number;				// E' la risposta dell'utente nel caso di task di tipo choose right option oppure enter value.
		private var uYNAnswer: String;				// E' la risposta dell'utente nel caso di task di tipo yes / no.
		private var key1: Button = new Button(Assets.getTexture("key"));
		private var key2: Button = new Button(Assets.getTexture("key"));
		private var key3: Button = new Button(Assets.getTexture("key"));
		private var key4: Button = new Button(Assets.getTexture("key"));
		private var key5: Button = new Button(Assets.getTexture("key"));
		private var key_plus: Button = new Button(Assets.getTexture("key"));
		private var key_minus: Button = new Button(Assets.getTexture("key"));
		private var key_divide: Button = new Button(Assets.getTexture("key"));
		private var key_times: Button = new Button(Assets.getTexture("key"));
		private var key_oBracket: Button = new Button(Assets.getTexture("key"));
		private var key_cBracket: Button = new Button(Assets.getTexture("key"));
		private var key_del: Button = new Button(Assets.getTexture("key"));
		private var key_ans: Button = new Button(Assets.getTexture("key"));
		private var equal: Button;
		private var key1Value: Number;
		private var key2Value: Number;
		private var key3Value: Number;
		private var key4Value: Number;
		private var key5Value: Number;
		private var keyField: flash.text.TextField;
		private var kBoard: Image;
		private var inputField: starling.text.TextField;
		private var inputField2: starling.text.TextField;
		private var inputField3: starling.text.TextField;
		private var inputText: String;
		private var inputValue: Number;
		private var bracketsValue: Number;
		private var yes: Button;
		private var no: Button;
		private var oBracket: Boolean;				// Indica se l'utente ha aperto una parentesi nell'inserimento della risposta in task di tipo enter value.
		private var simble: Boolean;				// Indica se l'utente ha inserito un operatore nell'inserimento della risposta in task di tipo enter value.
		private var theSimble: String;
		private var theBracketSimble: String;			// Indica il segno all'interno di parentesi.
		private var bracketSimble: Boolean;
		private var firstNumber: Boolean;			// Se true, non è ancora stato inserito un numero nella risposta a task di tipo enter value.
		private var firstBNumber: Boolean;
		private var answered: Boolean;				// Indica se l'utente ha fornito una risposta al task.
		private var Field: starling.text.TextField;
		private var expField: starling.text.TextField;
		private var timer: Timer;
		private var field: starling.text.TextField;
		private var text: String;
		private var taskId: String;
		private var format: TextFormat;
		private var buttDim: int;
		private var buttX: int;
		private var aButtY: int;
		private var o1ButtY: int;
		private var o2ButtY: int;
		private var addedToStage: Boolean;
		private var displayed: Boolean;
		private var error: Boolean;
		
		
		public function Notes(screen: String, aTask: String)
		{
			super();
			
			displayed = false;
			task = new Tasks();
			taskId = InnerClass.nextTask;
			
			nextScreen = screen;
			currentTask = aTask;
			simble = false;
			inputText = "";
			inputValue = 0;
			firstNumber = true;
			firstBNumber = true;
			error = false;
			uAnswer = 0;
			
			if(GameInfo.right)
				
				GameInfo.rAnswer = answer;
				
			else
				
				GameInfo.lAnswer = answer;
			
			addedToStage = false;
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			Game.database.addEventListener(ReadyEvent.TASK_SETTED, onReady);
		}
		
		private function onReady():void
		{
			if(task.taskSetted && addedToStage && !displayed)
			{
				displayNotes();
			}
		}
		
		private function onAddedToStage():void
		{
			addedToStage = true;
			
			// Se il task è già stato caricato, visualizzo il problema, altrimenti aspetto l'evento TASK_SETTED.
			
			if(task.taskSetted && !displayed)
			{
				displayNotes();
			}
				
			else
			{
				background = Game.loading;
				this.addChild(background);
			}
		}
		
		private function displayNotes():void
		{
			displayed = true;
			ynAnswer = task.ynAnswer;
			answer = Numbers.roundDecimal(task.answer, 3);
			option1 = Numbers.roundDecimal(task.option1, 3);
			option2 = Numbers.roundDecimal(task.option2, 2);
			yn = task.yn;
			cro = task.cro;
			var text: String = task.text;
			
			if(this.contains(background))
				
				this.removeChild(background);
			
			background = new Image(Assets.getTexture("bgNotes"));
			this.addChild(background);
			help = new Button(Assets.getTexture("helpButton"));
			help.x = 10;
			help.y = 510;
			this.addChild(help);
			
			textField = new starling.text.TextField(250, 600, text, "HoboStd", 17, Colors.PEN);
			textField.vAlign = VAlign.TOP;
			textField.x = 90;
			textField.y = 60;
			this.addChild(textField);
			
			if(!yn && !cro)
			{
				if(GameInfo.right)
					
					GameInfo.rType = Strings.EV;
					
				else
					
					GameInfo.lType = Strings.EV;
				
				keyboard();				
				writeExplaination();
				writeInput();
				
				equal = new Button(Assets.getTexture("equal"));
				equal.width = 100;
				equal.height = 100;
				equal.x = 520;
				equal.y = 310;
				this.addChild(equal);
				
				displayResult();
			}
				
			else if(yn)
			{
				if(GameInfo.right)
					
					GameInfo.rType = Strings.YN;
					
				else
					
					GameInfo.lType = Strings.YN;
				
				insertYN();
			}
				
			else
			{
				if(GameInfo.right)
					
					GameInfo.rType = Strings.CRO;
					
				else
					
					GameInfo.lType = Strings.CRO;
				
				insertOptions();
			}
			
			cont = new Button(Assets.getTexture("contButton"));
			cont.x = 630;
			cont.y = 440;
			cont.width = 150;
			cont.height = 150;
			this.addChild(cont);
			
			timer = new Timer(Game.timePrompt, 1);
			timer.start();
			
			timer.addEventListener(TimerEvent.TIMER, prompt);
			
			this.addEventListener(Event.TRIGGERED, onClick);
		}
		
		protected function prompt(event:TimerEvent):void
		{
			this.removeChildren();
			Starling.current.nativeOverlay.removeChildren();
			this.addChild(background);
			textField = new starling.text.TextField(250, 600, task.text, "HoboStd", 17, Colors.PEN);
			textField.vAlign = VAlign.TOP;
			textField.x = 90;
			textField.y = 60;
			this.addChild(textField);
			
			this.alpha = 0.8;
			this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.PROMPT}, true));
		}
		
		public function resetButtons(): void
		{
			if(yn)
			{
				this.addChild(yes);
				this.addChild(no);
			}
				
			else if(cro)
			{
				this.addChild(aButton);
				this.addChild(o1Button);
				this.addChild(o2Button);
				this.addChild(inputField);
				this.addChild(inputField2);
				this.addChild(inputField3);
			}
				
			else
			{
				Starling.current.nativeOverlay.removeChildren();
				writeInput();
				this.addChild(equal);
				writeExplaination();
				displayResult();
				keyboard();
			}
			
			this.addChild(help);
			this.addChild(cont);
		}
		
		private function writeExplaination(): void
		{
			var expText: String = "Make your calculation, click '=', then 'Continue'.";
			expField = new starling.text.TextField(250, 65, expText, "HoboStd", 15, Colors.PINK_PEN);
			expField.hAlign = HAlign.CENTER;
			expField.vAlign = VAlign.CENTER;
			expField.x = 445;
			expField.y = 15;
			this.addChild(expField);
		}
		
		private function writeInput():void
		{
			this.removeChild(inputField);
			inputField = new starling.text.TextField(250, 30, inputText, "HoboStd", 16, Colors.PEN);
			inputField.border = true;
			inputField.hAlign = HAlign.CENTER;
			inputField.vAlign = VAlign.CENTER;
			inputField.x = 445;
			inputField.y = 300;
			this.addChild(inputField);
		}
		
		private function insertOptions():void
		{
			aButton = new Button(Assets.getTexture("violet"));
			o1Button = new Button(Assets.getTexture("violet"));
			o2Button = new Button(Assets.getTexture("violet"));
			buttDim = 20;
			aButton.height = o1Button.height = o2Button.height = buttDim;
			aButton.width = o1Button.width = o2Button.width = buttDim;
			randomOptions();
			
			inputField = new starling.text.TextField(250, 20, answer.toString(), "HoboStd", 17, Colors.PEN);
			inputField.hAlign = HAlign.LEFT;
			inputField.vAlign = VAlign.CENTER;
			inputField.x = aButton.x + 50;
			inputField.y = aButton.y;
			this.addChild(inputField);
			
			inputField2 = new starling.text.TextField(250, 20, option1.toString(), "HoboStd", 17, Colors.PEN);
			inputField2.hAlign = HAlign.LEFT;
			inputField2.vAlign = VAlign.CENTER;
			inputField2.x = o1Button.x + 50;
			inputField2.y = o1Button.y;
			this.addChild(inputField2);
			
			inputField3 = new starling.text.TextField(250, 20, option2.toString(), "HoboStd", 17, Colors.PEN);
			inputField3.hAlign = HAlign.LEFT;
			inputField3.vAlign = VAlign.CENTER;
			inputField3.x = o2Button.x + 50;
			inputField3.y = o2Button.y;
			this.addChild(inputField3);
			
			this.addChild(aButton);
			this.addChild(o1Button);
			this.addChild(o2Button);
		}
		
		private function randomOptions(): void
		{
			var n: int = Math.floor(Math.random() * 8 + 1);
			
			buttX = 450;
			var y1: int = 100;
			var y2: int = 200;
			var y3: int = 300;
			
			aButton.x = o1Button.x = o2Button.x = buttX;
			
			switch(n)
			{
				case 1:
				{
					aButton.y = aButtY = y1;
					o1Button.y = o1ButtY = y2;
					o2Button.y = o2ButtY = y3;
					
					break;
				}
					
				case 2:
				{
					aButton.y = aButtY = y1;
					o1Button.y = o1ButtY = y3;
					o2Button.y = o2ButtY = y2;
					
					break;
				}
					
				case 3:
				{
					aButton.y = aButtY = y2;
					o1Button.y = o1ButtY = y1;
					o2Button.y = o2ButtY = y3;
					
					break;
				}
					
				case 4:
				{
					aButton.y = aButtY = y2;
					o1Button.y = o1ButtY = y3;
					o2Button.y = o2ButtY = y1;
					
					break;
				}
					
				case 5:
				{
					aButton.y = aButtY = y3;
					o1Button.y = o1ButtY = y1;
					o2Button.y = o2ButtY = y2;
					
					break;
				}
					
				default:
				{
					aButton.y = aButtY = y3;
					o1Button.y = o1ButtY = y2;
					o2Button.y = o2ButtY = y1;
					
					break;
				}
			}
		}
		
		private function insertYN():void
		{
			yes = new Button(Assets.getTexture("yes"));
			no = new Button(Assets.getTexture("no"));
			yes.width = 100;
			yes.height = 100;
			no.width = yes.width;
			no.height = yes.height;
			yes.x = 445;
			yes.y = 230;
			no.x = yes.x + 150;
			no.y = yes.y;
			this.addChild(yes);
			this.addChild(no);
		}
		
		private function keyboard():void
		{
			kBoard = new Image(Assets.getTexture("keyboard"));
			kBoard.height = 180;
			kBoard.width = 340;
			kBoard.x = 400;
			kBoard.y = 205;
			//this.addChild(kBoard);
			
			
			switch(taskId)
			{
				case "task04": case "task05":
				{
					key1Value = Tasks.n1;
					key2Value = Tasks.n2;
					
					break;
				}
					
				case "task09": case "task18":
				{
					key1Value = Tasks.p1;
					key2Value = Tasks.p2;
					
					break;
				}
					
				case "task10":
				{
					key1Value = Tasks.n1;
					key2Value = Tasks.n2;
					key3Value = Tasks.n3;
					key4Value = Tasks.p1;
					
					break;
				}
					
				case "task14": case "task20": case "task22": case "task24": 
				{
					key1Value = Tasks.p1;
					key2Value = Tasks.p2;
					key3Value = Tasks.p3;
					key4Value = Tasks.n1;
					
					break;
				}
					
				case "task15":
				{
					key1Value = Tasks.p1;
					key2Value = Tasks.p2;
					key3Value = Tasks.n1;
					key4Value = Tasks.n2;
					key5Value = Tasks.n3;
					
					break;
				}
					
				case "task19":
				{
					key1Value = Tasks.n1;
					key2Value = Tasks.n2;
					key3Value = Tasks.n3;
					key4Value = Tasks.p1;
					
					break;
				}
					
				case "task25":
				{
					key1Value = Tasks.p1;
					key2Value = Tasks.p2;
					key3Value = Tasks.p3;
					key4Value = Tasks.p4;
					
					break;
				}
			}
			
			inizializeButton(key1, key1Value.toString(), 1);
			inizializeButton(key2, key2Value.toString(), 2);
			
			switch(taskId)
			{
				case "task10": case "task14": case "task19": case "task20": case "task22": case "task24": case "task25":
				{
					inizializeButton(key3, key3Value.toString(), 3);
					inizializeButton(key4, key4Value.toString(), 4);
					
					break
				}
					
				case "task15":
				{
					inizializeButton(key3, key3Value.toString(), 3);
					inizializeButton(key4, key4Value.toString(), 4);
					inizializeButton(key5, key5Value.toString(), 13);
				}
			}
			
			inizializeButton(key_plus, Strings.PLUS, 5);
			inizializeButton(key_minus, Strings.MINUS, 6);
			inizializeButton(key_times, Strings.TIMES, 7);
			inizializeButton(key_divide, Strings.DIVIDE, 8);
			inizializeButton(key_oBracket, Strings.O_BRACKET, 9);
			inizializeButton(key_cBracket, Strings.C_BRACKET, 10);
			inizializeButton(key_del, Strings.DEL, 11);
			inizializeButton(key_ans, Strings.ANS, 12);
		}
		
		private function inizializeButton(butt: Button, val: String, n: int):void
		{
			switch(n)
			{
				case 1:
				{
					switch(taskId)
					{
						case "task19": case "task25": case "task10": case "task14": case "task20": case "task22": case "task24":
						{
							butt.x = 420;	
							break;
						}
							
						case "task15": 
						{
							butt.x = 400;	
							break;
						}
							
							/*case "task10": case "task14": case "task15": case "task20": case "task22": case "task24":
							{
							butt.x = 460;	
							break;
							}*/
							
						default:
						{
							butt.x = 500;
							break;
						}
					}
					
					butt.y = 80;					
					
					break
				}
					
				case 2:
				{
					switch(taskId)
					{
						case "task19": case "task25": case "task10": case "task14": case "task20": case "task22": case "task24":
						{
							butt.x = 500;	
							break;
						}
							
						case "task15": 
						{
							butt.x = 470;	
							break;
						}
							
							/*case "task10": case "task14": case "task15": case "task20": case "task22": case "task24":
							{
							butt.x = 540;	
							break;
							}*/
							
						default:
						{
							butt.x = 580;
							break;
						}
					}
					
					butt.y = 80;
					
					break
				}
					
				case 3:
				{
					switch(taskId)
					{
						case "task19": case "task25": case "task10": case "task14": case "task20": case "task22": case "task24":
						{
							butt.x = 580;
							break;
						}
							
						case "task15": 
						{
							butt.x = 540;	
							break;
						}
							
							/*case "task10": case "task14": case "task15": case "task20": case "task22": case "task24":
							{
							butt.x = 620;	
							break;
							}	*/
					}
					
					butt.y = 80;
					
					break
				}
					
				case 4:
				{
					switch(taskId)
					{
						case "task19": case "task25": case "task10": case "task14": case "task20": case "task22": case "task24":
						{
							butt.x = 660;	
							break;
						}
							
						case "task15": 
						{
							butt.x = 610;	
							break;
						}
							
							/*	case "task10": case "task14": case "task15": case "task20": case "task22": case "task24":
							{
							butt.x = 420;	
							break;
							}	*/
					}
					
					butt.y = 80;
					
					break
				}
					
				case 5:
				{
					butt.x = 420;
					butt.y = 150;
					
					break
				}
					
				case 6:
				{
					butt.x = 500;
					butt.y = 150;
					
					break
				}
					
				case 7:
				{
					butt.x = 580;
					butt.y = 150;
					
					break
				}
					
				case 8:
				{
					butt.x = 660;
					butt.y = 150;
					
					break
				}
					
				case 9:
				{
					butt.x = 420;
					butt.y = 220;
					
					break
				}
					
				case 10:
				{
					butt.x = 500;
					butt.y = 220;
					
					break
				}
					
				case 11:
				{
					butt.x = 580;
					butt.y = 220;
					
					break
				}
					
				case 12:
				{
					butt.x = 660;
					butt.y = 220;
					
					break
				}
					
				case 13: 
				{
					butt.x = 680;	
					butt.y = 80;
					
					break;
				}
			}
			
			butt.width = 60;
			butt.height = 60;
			this.addChild(butt);
		/*	
			keyField = new TextField(butt.width, 20, val, "HoboStd", 17, Colors.PEN);
			keyField.hAlign = HAlign.CENTER;
			keyField.vAlign = VAlign.CENTER;
			keyField.x = butt.x;
			keyField.y = butt.y + (butt.height / 2) - 10;
			this.addChild(keyField);
		*/
			
			format = new TextFormat();
			
			if(n == 11)
				
				format.color = Colors.PINK_PEN;
			
			else
				
				format.color = Colors.PEN;
			
			//format.font = "HoboStd";
			format.size = 17;
			format.align = TextFormatAlign.CENTER;
			
			keyField = new flash.text.TextField();
			keyField.defaultTextFormat = format;
			keyField.mouseEnabled = false;
			keyField.width = butt.width;
			keyField.height = 20;
			keyField.text = val;
			
				//keyField = new Testarling.text.TextFieldutt.width, 20, val, "HoboStd", 17, Colors.PEN);
				
			keyField.x = butt.x;
			keyField.y = butt.y + (butt.height / 2) - 10;
			Starling.current.nativeOverlay.addChild(keyField);
		}
		
		private function onClick(event: Event):void
		{
			var buttonClicked: Button = event.target as Button;
			
			if(error && (buttonClicked as Button) == key_del)
			{
				resetKeyboard();
				inputText = "";
				writeInput();
				this.removeChild(inputField2);
			}
			
			else if(!error)
			{
				if((buttonClicked as Button) == key1)
				{
					tempValue(key1Value.toString());
					
					if(!error)
						
						inputText += key1Value;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key2)
				{
					tempValue(key2Value.toString());				
					
					if(!error)
						
						inputText += key2Value;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key3)
				{
					tempValue(key3Value.toString());
					
					if(!error)
						
						inputText += key3Value;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key4)
				{
					tempValue(key4Value.toString());
					
					if(!error)
						
						inputText += key4Value;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key5)
				{
					tempValue(key5Value.toString());
					
					if(!error)
						
						inputText += key5Value;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key_plus)
				{
					tempValue(Strings.PLUS);
					
					if(!error)
						
						inputText += Strings.PLUS;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key_minus)
				{
					tempValue(Strings.MINUS);
					
					if(!error)
					
						inputText += Strings.MINUS;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key_times)
				{
					tempValue(Strings.TIMES);
					
					if(!error)
						
						inputText += Strings.TIMES;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key_divide)
				{
					tempValue(Strings.DIVIDE);
					
					if(!error)
						
						inputText += Strings.DIVIDE;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key_oBracket)
				{
					tempValue(Strings.O_BRACKET);
					
					if(!error)
						
						inputText += Strings.O_BRACKET;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key_cBracket)
				{
					tempValue(Strings.C_BRACKET);
					
					if(!error)
						
						inputText += Strings.C_BRACKET;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == key_del)
				{
					resetKeyboard();
					inputText = "";
					writeInput();
				}
					
				else if((buttonClicked as Button) == key_ans)
				{
					tempValue("ans");
					
					if(!error)
						
						inputText += uAnswer;
					
					writeInput();
				}
					
				else if((buttonClicked as Button) == equal)
				{
					timer.removeEventListener(TimerEvent.TIMER, prompt);
					
					if(oBracket || simble)
					{
						resetKeyboard();
						displayError("There is a bracket or an operator too much.");
						writeInput();
					}
						
					else
					{
						uAnswer = inputValue;
						inputText = uAnswer.toString();
						displayResult();
						
						if(!firstNumber)
							
							answered = true;
					}
				}
					
				else if((buttonClicked as Button) == yes)
				{
					timer.removeEventListener(TimerEvent.TIMER, prompt);
					
					this.removeChild(yes);
					yes.width = 90;
					yes.height = 90;
					yes.x = 450;
					yes.y = 235;
					this.addChild(yes);
					this.removeChild(no);
					no.width = 100;
					no.height = 100;
					no.x = 595;
					no.y = 230;
					this.addChild(no);					
					
					uYNAnswer = Strings.YES;
					answered = true;
				}
					
				else if((buttonClicked as Button) == no)
				{
					timer.removeEventListener(TimerEvent.TIMER, prompt);
					
					this.removeChild(yes);
					yes.width = 100;
					yes.height = 100;
					yes.x = 445;
					yes.y = 230;
					this.addChild(yes);
					this.removeChild(no);
					no.width = 90;
					no.height = 90;
					no.x = 600;
					no.y = 235;
					this.addChild(no);
					
					uYNAnswer = Strings.NO;
					answered = true;
				}
					
				else if((buttonClicked as Button) == aButton)
				{
					timer.removeEventListener(TimerEvent.TIMER, prompt);
					
					this.removeChild(aButton);
					this.removeChild(o1Button);
					this.removeChild(o2Button);
					aButton = new Button(Assets.getTexture("blue"));
					aButton.y = aButtY;
					o1Button = new Button(Assets.getTexture("violet"));
					o1Button.y = o1ButtY;
					o2Button = new Button(Assets.getTexture("violet"));
					o2Button.y = o2ButtY;
					aButton.x = o1Button.x = o2Button.x = buttX;
					aButton.height = o1Button.height = o2Button.height = buttDim;
					aButton.width = o1Button.width = o2Button.width = buttDim;
					this.addChild(aButton);
					this.addChild(o1Button);
					this.addChild(o2Button);
					
					uAnswer = answer;
					answered = true;
				}
					
				else if((buttonClicked as Button) == o1Button)
				{
					timer.removeEventListener(TimerEvent.TIMER, prompt);
					
					this.removeChild(aButton);
					this.removeChild(o1Button);
					this.removeChild(o2Button);
					aButton = new Button(Assets.getTexture("violet"));
					aButton.y = aButtY;
					o1Button = new Button(Assets.getTexture("blue"));
					o1Button.y = o1ButtY;
					o2Button = new Button(Assets.getTexture("violet"));
					o2Button.y = o2ButtY;
					aButton.x = o1Button.x = o2Button.x = buttX;
					aButton.height = o1Button.height = o2Button.height = buttDim;
					aButton.width = o1Button.width = o2Button.width = buttDim;
					this.addChild(aButton);
					this.addChild(o1Button);
					this.addChild(o2Button);
					
					uAnswer = option1;
					answered = true;
				}
					
				else if((buttonClicked as Button) == o2Button)
				{
					timer.removeEventListener(TimerEvent.TIMER, prompt);
					
					this.removeChild(aButton);
					this.removeChild(o1Button);
					this.removeChild(o2Button);
					aButton = new Button(Assets.getTexture("violet"));
					aButton.y = aButtY;
					o1Button = new Button(Assets.getTexture("violet"));
					o1Button.y = o1ButtY;
					o2Button = new Button(Assets.getTexture("blue"));
					o2Button.y = o2ButtY;
					aButton.x = o1Button.x = o2Button.x = buttX;
					aButton.height = o1Button.height = o2Button.height = buttDim;
					aButton.width = o1Button.width = o2Button.width = buttDim;
					this.addChild(aButton);
					this.addChild(o1Button);
					this.addChild(o2Button);
					
					uAnswer = option2;
					answered = true;
				}
					
				else if((buttonClicked as Button) == help)
				{
					this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.HELP}, true));
				}
					
				else if((buttonClicked as Button) == cont)
				{
					if(answered)
					{
						Starling.current.nativeOverlay.removeChildren();
						
						if(GameInfo.right && !yn)
						{
							if((taskId == "task02") || (taskId == "task07") || (taskId == "task21") || (taskId == "task22"))
								
								GameInfo.rightProb = Numbers.roundDecimal((1 - uAnswer), 3);
								
							else
								
								GameInfo.rightProb = Numbers.roundDecimal(uAnswer, 3);
							
							if(answer - Game.tollerance <= uAnswer && uAnswer <= answer + Game.tollerance)
							{
								GameInfo.commit = true;
								GameInfo.rCorrect = true; 
							}
								
							else
							{
								GameInfo.commit = false;
								GameInfo.rCorrect = false;
								
								//Game.time += Game.wrongAns;
							}
						}
							
						else if(GameInfo.right && yn)
						{
							GameInfo.rightYN = uYNAnswer;
							
							if(uYNAnswer == ynAnswer)
							{
								GameInfo.commit = true;
								GameInfo.rCorrect = true;
							}
								
							else
							{
								GameInfo.commit = false;
								GameInfo.rCorrect = false;
								
								//Game.time += Game.wrongAns;
							}
						}
							
						else if(!GameInfo.right && !yn)
						{
							if((taskId == "task02") || (taskId == "task07") || (taskId == "task21") || (taskId == "task22"))
								
								GameInfo.leftProb = Numbers.roundDecimal((1 - uAnswer), 3);
								
							else
								
								GameInfo.leftProb = Numbers.roundDecimal(uAnswer, 3);
							
							if(answer - Game.tollerance <= uAnswer && uAnswer <= answer + Game.tollerance)
							{
								GameInfo.commit = true;
								GameInfo.lCorrect = true;
							}
								
							else
							{
								GameInfo.commit = false;
								GameInfo.lCorrect = false;
								
								//Game.time += Game.wrongAns;
							}
						}
							
						else
						{
							GameInfo.leftYN = uYNAnswer;
							
							if(uYNAnswer == ynAnswer)
							{
								GameInfo.commit = true;
								GameInfo.lCorrect = true; 
							}
								
							else
							{
								GameInfo.commit = false;
								GameInfo.lCorrect = false;
								
								//Game.time += Game.wrongAns;
							}
						}
						
						setText();
						
						Starling.current.nativeOverlay.removeChildren();
						this.removeChildren();
						this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.CROSSROADS}, true));		
					}				
				}
			}
		}
		
		private function setText():void
		{
			if(GameInfo.right && GameInfo.rType == Strings.YN)
			{
				if(GameInfo.rightYN == Strings.YES)
					
					GameInfo.rString = "p > 0.5";
					
				else
					
					GameInfo.rString = "p < 0.5";
			}
				
			else if(GameInfo.right)
			{
				GameInfo.rString = "p = " + GameInfo.rightProb;
			}
				
			else if(!GameInfo.right && GameInfo.lType == Strings.YN)
			{
				if(GameInfo.leftYN == Strings.YES)
					
					GameInfo.lString = "p > 0.5";
					
				else
					
					GameInfo.lString = "p < 0.5";
			}
				
			else if(!GameInfo.right)
			{
				GameInfo.lString = "p = " + GameInfo.leftProb;
			}
		}
		
		private function displayResult():void
		{
			this.removeChild(Field)
			Field = new starling.text.TextField(250, 30, inputValue.toString(), "HoboStd", 17, Colors.PEN);
			Field.border = true;
			Field.hAlign = HAlign.CENTER;
			Field.vAlign = VAlign.CENTER;
			Field.x = 445;
			Field.y = 390;
			this.addChild(Field);
		}
		
		private function tempValue(clicked: String):void
		{
			switch(clicked)
			{
				case key1Value.toString(): case key2Value.toString(): case key3Value.toString(): case key4Value.toString(): case key5Value.toString():
				{
					if(firstNumber && !simble && !oBracket)
					{
						inputValue = Number(clicked);
						firstNumber = false;
					}
						
					else if(!firstNumber && simble && !oBracket)
					{
						if(theSimble == Strings.PLUS)
						{
							inputValue += Number(clicked);
						}
							
						else if(theSimble == Strings.MINUS)
						{
							inputValue -= Number(clicked);
						}
							
						else if(!oBracket && theSimble == Strings.TIMES)
						{
							inputValue *= Number(clicked);
						}
							
						else if(!oBracket && theSimble == Strings.DIVIDE)
						{
							inputValue /= Number(clicked);
						}
						
						simble = false;
					}
						
					else if(firstBNumber && !bracketSimble && oBracket)
					{
						bracketsValue = Number(clicked);
						firstBNumber = false;
					}
						
					else if(!firstBNumber && bracketSimble && oBracket)
					{
						if(theBracketSimble == Strings.PLUS)
						{
							bracketsValue += Number(clicked);
						}
							
						else if(theBracketSimble == Strings.MINUS)
						{
							bracketsValue -= Number(clicked);
						}
							
						else if(theBracketSimble == Strings.TIMES)
						{
							bracketsValue *= Number(clicked);
						}
							
						else if(theBracketSimble == Strings.DIVIDE)
						{
							bracketsValue /= Number(clicked);
						}
						
						bracketSimble = false;
					}
						
					else
					{
						resetKeyboard();
						displayError("You can not type two numbers consecutively.");
					}
					
					break;
				}
					
				case "ans":
				{
					if(firstNumber && !simble && !oBracket)
					{
						inputValue = uAnswer;
						firstNumber = false;
					}
						
					else if(!firstNumber && simble && !oBracket)
					{
						if(theSimble == Strings.PLUS)
						{
							inputValue += uAnswer;
						}
							
						else if(theSimble == Strings.MINUS)
						{
							inputValue -= uAnswer;
						}
							
						else if(!oBracket && theSimble == Strings.TIMES)
						{
							inputValue *= uAnswer;
						}
							
						else if(!oBracket && theSimble == Strings.DIVIDE)
						{
							inputValue /= uAnswer;
						}
						
						simble = false;
					}
						
					else if(firstBNumber && !bracketSimble && oBracket)
					{
						bracketsValue = uAnswer;
						firstBNumber = false;
					}
						
					else if(!firstBNumber && bracketSimble && oBracket)
					{
						if(theBracketSimble == Strings.PLUS)
						{
							bracketsValue += uAnswer;
						}
							
						else if(theBracketSimble == Strings.MINUS)
						{
							bracketsValue -= uAnswer;
						}
							
						else if(theBracketSimble == Strings.TIMES)
						{
							bracketsValue *= uAnswer;
						}
							
						else if(theBracketSimble == Strings.DIVIDE)
						{
							bracketsValue /= uAnswer;
						}
						
						bracketSimble = false;
					}
						
					else
					{
						resetKeyboard();
						displayError("You can not type two numbers consecutively.");
					}
					
					break;
				}
					
				case Strings.PLUS:
				{
					if(!oBracket && !firstNumber && !simble)
					{
						simble = true;
						theSimble = Strings.PLUS;
					}
						
					else if(oBracket && !firstBNumber && !bracketSimble)
					{
						bracketSimble = true;
						theBracketSimble = Strings.PLUS
					}
						
					else
					{
						resetKeyboard();
						displayError("An operator has always to be between two numbers.");
					}
					
					break;
				}
					
				case Strings.MINUS:
				{
					if(!oBracket && !firstNumber && !simble)
					{
						simble = true;
						theSimble = Strings.MINUS;
					}
						
					else if(oBracket && !firstBNumber && !bracketSimble)
					{
						bracketSimble = true;
						theBracketSimble = Strings.MINUS;
					}
						
					else
					{
						resetKeyboard();
						displayError("An operator has always to be between two numbers.");
					}
					
					break;
				}
					
				case Strings.TIMES:
				{
					if(!oBracket && !firstNumber && !simble)
					{
						simble = true;
						theSimble = Strings.TIMES;
					}
						
					else if(oBracket && !firstBNumber && !bracketSimble)
					{
						bracketSimble = true;
						theBracketSimble = Strings.TIMES;
					}
						
					else
					{
						resetKeyboard();
						displayError("An operator has always to be between two numbers.");
					}
					
					break;
				}
					
				case Strings.DIVIDE:
				{
					if(!oBracket && !firstNumber && !simble)
					{
						simble = true;
						theSimble = Strings.DIVIDE;
					}
						
					else if(oBracket && !firstBNumber && !bracketSimble)
					{
						bracketSimble = true;
						theBracketSimble = Strings.DIVIDE;
					}
						
					else
					{
						resetKeyboard();
						displayError("An operator has always to be between two numbers.");
					}
					
					break;
				}
					
				case Strings.O_BRACKET:
				{
					if(!oBracket && ((firstNumber && !simble) || (!firstNumber && simble)))
					{
						oBracket = true;
						firstBNumber = true;
					}
						
					else
					{
						resetKeyboard();
						displayError("You have already opened a bracket");
					}
					
					break;
				}
					
				case Strings.C_BRACKET:
				{
					if(oBracket)
					{
						oBracket = false;
						firstNumber = false;
						
						if(simble)
						{
							simble = false;
							
							// Se sono qui è perchè ho un valore, un operando e quindi le parentesi. Devo quindi fare l'operazione.
							
							switch(theSimble)
							{
								case Strings.PLUS:
								{
									inputValue += bracketsValue;
									bracketsValue = 0;
									
									break;
								}
									
								case Strings.MINUS:
								{
									inputValue -= bracketsValue;
									bracketsValue = 0;
									
									break;
								}
									
								case Strings.TIMES:
								{
									inputValue *= bracketsValue;
									bracketsValue = 0;
									
									break;
								}
									
								case Strings.DIVIDE:
								{
									inputValue /= bracketsValue;
									bracketsValue = 0;
									
									break;
								}
							}
						}
							
						else
						{
							// Se sono qui è perchè il valore fra parentesi non è preceduto da altri valori, quindi non devo fare operazioni.
							
							inputValue = bracketsValue;
						}
					}
						
					else
					{
						resetKeyboard();
						displayError("You have not opened a bracket.");
					}
					
					break;
				}
					
				default:
				{
					text = "DEFAULT!!!!";
					field = new starling.text.TextField(200, 200, text, "HoboStd", 20, Colors.PINK_PEN);
					field.border = true;
					field.hAlign = HAlign.CENTER;
					field.vAlign = VAlign.CENTER;
					field.border = true;
					field.x = 200;
					field.y = 400;
					this.addChild(field);
					
					break;
				}
			}
		}
		
		private function resetKeyboard():void
		{
			inputValue = 0;
			bracketsValue = 0;
			inputText = "Error: click 'del' and try again.";
			oBracket = false;
			simble = false;
			bracketSimble = false;
			firstNumber = true;
			firstBNumber = true;
			error = false;
		}
	
		private function displayError(theText: String): void
		{
			inputField2 = new starling.text.TextField(200, 100, theText, "HoboStd", 16, Colors.PINK_PEN);
			inputField2.hAlign = HAlign.CENTER;
			inputField2.vAlign = VAlign.CENTER;
			inputField2.x = 415;
			inputField2.y = 440;
			inputField2.border = true;
			this.addChild(inputField2);
			error = true;
		}
	}
}