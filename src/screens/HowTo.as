package screens
{
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import events.NavigationEvent;
	
	import objects.Strings;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	
	public class HowTo extends Sprite
	{
		private var background: Image;
		private var girl: Image;
		private var bubble: Image;
		private var cont: Button;
		private var skip: Button;
		private var letSGo: Button;
		private var home: Button;
		private static var n: int = 1;
		private var textField: TextField;
		private static var text: String;
		private var url: URLRequest;
		private var loader: URLLoader;
		
		
		public function HowTo()
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{			
			//background = new Image(Assets.getTexture("bgLake"));
			background = new Image(Assets.getTexture("howTo"));
			this.addChild(background);
			
			girl = new Image(Assets.getTexture("girl1"));
			girl.width = 427;
			girl.height = 800;
			girl.x = 400;
			girl.y = 100;
			this.addChild(girl);
			
			bubble = new Image(Assets.getTexture("bubble2"));
			bubble.width = 400;
			bubble.height = 300;
			bubble.x = 20;
			bubble.y = 20;
			this.addChild(bubble);
			
			cont = new Button(Assets.getTexture("contButton"));
			cont.width = cont.height = 150;
			cont.x = 20;
			this.addChild(cont);
			
			skip = new Button(Assets.getTexture("skipButton"));
			skip.width = skip.height = 120;
			skip.x = 180;
			this.addChild(skip);
			
			home = new Button(Assets.getTexture("home"));
			home.x = 600;
			home.y = 25;
			home.width = home.height = 70;
			this.addChild(home);
			
			text = "Hi! You are the head of a group of hikers heading to the top of the mountain.";
			//readText("text.txt");
			writeText(text);
			
			this.addEventListener(Event.TRIGGERED, onButtonClick);
		}	
		
		private function onButtonClick(event: Event):void
		{
			var buttonClicked: Button = event.target as Button;
			
			if((buttonClicked as Button) == cont)
			{
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.CONTINUE}, true));
			}
				
			else if((buttonClicked as Button) == skip)
			{
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.SKIP}, true));
			}
				
			else if((buttonClicked as Button) == letSGo)
			{
				this.visible = false;
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.LETSGO}, true));
			}
			
			else if((buttonClicked as Button) == home)
			{
				this.visible = false;
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.BACK_HOME}, true));
			}
		}
		
		private function writeText(text:String):void
		{
			textField = new TextField(300, 200, text, "HoboStd", 25, 0x003399);
			textField.x = 60;
			textField.y = 50;
			this.addChild(textField);			
		}
		
		public function update(): void
		{
			switch(n)
			{
				case 1:
				{
					text = "You have to lead the group to your destination as fast as possible.";
					n++;
					
					break;
				}
					
				case 2:
				{
					text = "There are several possible itineraries; some shorter, some longer.";
					n++;
					
					break;	
				}
					
				case 3:
				{
					text = "You have to use your knowledge to calculate the probability of each way and make your decisions.";
					n++;
					
					break;	
				}
					
				case 4:
				{
					text = "It is possible to choose a way with low probability of success in order to save time...";
					n++;
					
					break;	
				}
					
				case 5:
				{
					text = "But be careful with your choices!";
					n++;
					
					break;	
				}
					
				case 6:
				{
					text = "If you can't overcome an obstacle, you will have to come back and you will waste time!";
					n++;
					
					break;	
				}
					
				case 7:
				{
					text = "Don’t worry if you can’t remember all the formulas:";
					n++;
					
					break;	
				}
					
				case 8:
				{
					text = "you can look them up by clicking on the book icon on the bottom left of the screen.";
					n++;
					
					break;	
				}
					
				default:
				{
					letSGo = new Button(Assets.getTexture("letSGoButton"));
					letSGo.x = 20;
					letSGo.y = 410;
					this.addChild(letSGo);
					
					this.removeChild(cont);
					this.removeChild(skip);
					
					text = "Will you succeed in your mission? ";
					n++;
					
					break;
				}
			}
			
			this.removeChild(textField);
			writeText(text);
		}
		
		public function inizialize():void
		{
			this.visible = true;
			this.addEventListener(Event.ENTER_FRAME, animations);
		}
		
		private function animations():void
		{
			var date: Date = new Date();
			cont.y = 430 + (Math.cos(date.getTime() * 0.002) * 10);
			skip.y = 370 + (Math.cos(date.getTime() * 0.002) * 10);
		}
	}
}