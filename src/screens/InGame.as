package screens
{
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	import events.NavigationEvent;
	import objects.GameBackground;
	import objects.Strings;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import inner_class.InnerClass;
	
	
	public class InGame extends Sprite
	{
		private var bg: GameBackground;
		
		private var textField: TextField;
		private var response: String;
		private static var timer: Timer;
		
		
		public function InGame()
		{
			super();
			
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event: Event):void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			
			drawGame();
		}
		
		public function drawGame():void
		{
			var date: Date = new Date;
			
			bg = new GameBackground();
			bg.speed = 10;
			this.addChild(bg);
		}
		
		public function disposeTemporarily(): void
		{
			this.visible = false;
		}
		
		public function inizialize():void
		{
			this.visible = true;
			
			GameInfo.ResetInfo();
			
			if(Game.webService)
				
				Game.tuGraz.interact(Strings.NEXT_TASK);
			
			else
				
				InnerClass.loadNextTask();
			
			timer = new Timer(Game.timeWalking, 10);
			timer.start();
			
			timer.addEventListener(TimerEvent.TIMER, crossroads);
		}
		
		public function crossroads(event: TimerEvent): void
		{
			if((Game.webService && Game.next) || !Game.webService)
			{
				timer.stop();
				
				this.visible = false;
				
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.CROSSROADS}, true));
			}
		}
	}
}