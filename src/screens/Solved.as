package screens
{
	import flash.events.AccelerometerEvent;
	import flash.events.Event;
	
	import events.NavigationEvent;
	import objects.Strings;
	import objects.Colors;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	public class Solved extends Sprite
	{
		private var bg: Image;
		private var text: String;
		private var textField: TextField;
		private var textField2: TextField;
		private var back: Button;
		
		
		public function Solved()
		{
			super();
			
			this.addEventListener(flash.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			bg = new Image(Assets.getTexture("bgNotes"));
			this.addChild(bg);
			
			text = "Problem already solved!!";
			textField = new TextField(238, 276, text, "HoboStd", 30, 0xFF0066);
			textField.x = 450;
			textField.y = 95;
			this.addChild(textField);
			
			if(GameInfo.right)
				
				text = GameInfo.rString;
				
			else
				
				text = GameInfo.lString;
			
			textField2 = new TextField(238, 276, text, "HoboStd", 30, Colors.PEN);
			textField2.x = 450;
			textField2.y = 200;
			this.addChild(textField2);
			
			back = new Button(Assets.getAtlas().getTexture("backButton"));
			back.x = 20;
			back.y = 430;
			this.addChild(back);
			
			this.addEventListener(starling.events.Event.TRIGGERED, onClick);
		}
		
		private function onClick():void
		{
			this.visible = false;
			this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.CROSSROADS}, true));
		}
	}
}