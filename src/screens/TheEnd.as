package screens
{
	import objects.Colors;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import inner_class.InnerClass;
	
	public class TheEnd extends Sprite
	{
		private var bg: Image;
		private var congrats: Image;
		private var youHaveReached: Image;
		private var theTopOfThe: Image;
		private var highestMountain: Image;
		private var scoreField: TextField;
		private var comment: String;
		private var commentField: TextField;
		
		
		public function TheEnd()
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			bg = new Image(Assets.getTexture("bgEnd"));
			this.addChild(bg);
			
			scoreField = new TextField(350, 50, "Minutes: " + Game.time, "HoboStd", 40, Colors.PINK_PEN);
			scoreField.hAlign = HAlign.LEFT;
			scoreField.vAlign = VAlign.TOP;
			scoreField.x = 550;
			scoreField.y = 0;
			this.addChild(scoreField);
			
			congrats = new Image(Assets.getTexture("congrats"));
			congrats.width = 729;
			congrats.height = 108;
			congrats.x = 35;
			congrats.y = 70;
			this.addChild(congrats);
			
			youHaveReached = new Image(Assets.getTexture("youHaveReached"));
			youHaveReached.width = 772;
			youHaveReached.height = 90;
			youHaveReached.x = 14;
			youHaveReached.y = congrats.y + congrats.height + 20;
			this.addChild(youHaveReached);
			
			theTopOfThe = new Image(Assets.getTexture("theTopOfThe"));
			theTopOfThe.width = 622;
			theTopOfThe.height = 107;
			theTopOfThe.x = 89;
			theTopOfThe.y = youHaveReached.y + youHaveReached.height;
			this.addChild(theTopOfThe);
			
			highestMountain = new Image(Assets.getTexture("highestMountain"));
			highestMountain.width = 783;
			highestMountain.height = 108;
			highestMountain.x = 8;
			highestMountain.y = theTopOfThe.y + theTopOfThe.height - 10;
			this.addChild(highestMountain);
			
			if(Game.time > 10000)
				
				comment = "Maybe you could have gone faster...";
				
			else
				
				comment = "You have been really quick... And lucky!";
			
			commentField = new TextField(800, 100, comment, "HoboStd", 30, Colors.PINK_PEN);
			commentField.hAlign = HAlign.CENTER;
			commentField.vAlign = VAlign.CENTER;
			commentField.x = 10;
			commentField.y = 500;
			this.addChild(commentField);
			
			if(!Game.webService)
			{
				trace("\nLast p values:");
				InnerClass.traceValues();
			}
			
		}
	}
}