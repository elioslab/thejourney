package screens
{
	import events.NavigationEvent;
	
	import objects.Strings;
	import objects.Colors;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import inner_class.InnerClass;
	
	
	public class Prompt extends Sprite
	{
		private var task: String;
		private var image1: Image;
		private var text: String;
		private var textField: TextField;
		private var txtField1: TextField;
		private var txtField2: TextField;
		private var txtField3: TextField;
		private var txtField4: TextField;
		private var color: uint;
		private var back: Button;
		
		
		public function Prompt()
		{
			super();
			
			if(Game.webService)
				
				task = TuGraz.nextTask;
				
			else
				
				task = InnerClass.nextTask;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			this.removeChildren();
			setImage(405, 120, 318, 230);
			setTxt();
			
			back = new Button(Assets.getAtlas().getTexture("backButton"));
			back.x = 20;
			back.y = 430;
			this.addChild(back);
			
			this.addEventListener(Event.TRIGGERED, onBack);
		}
		
		private function onBack():void
		{
			Game.time += Game.costPerPrompt;
			/*
			this.removeChild(image1);
			this.removeChild(textField);
			this.visible = false;
			this.removeChildren(); */
			image1.parent.removeChild(image1);
			textField.parent.removeChild(textField);
			this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.BACK_PROMPT}, true));
			this.removeFromParent();
		}
		
		private function setTxt():void
		{
			switch(task)
			{
				case "task01": case "task02": case "task03": case "task04": case "task05":
				{
					txtField1 = new TextField(100, image1.height, "x / n", "HoboStd", 25, Colors.PINK);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 2) + 30;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(125, image1.height, "( n - x) / n", "HoboStd", 25, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 2) - 145;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.BLUE;
					
					break;
				}
					
				case "task06": case "task07": case "task10":
				{
					txtField1 = new TextField(115, image1.height, "P(highT)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 88;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(115, image1.height, "P(snow)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 48;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task08":
				{
					txtField1 = new TextField(115, image1.height, "P(cow)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 88;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(snow)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 48;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task09":
				{
					txtField1 = new TextField(115, image1.height, "P(hot)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 88;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(cANDs)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 48;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task11": case "task12":
				{
					txtField1 = new TextField(115, image1.height, "P(wind)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 98;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(rain)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 58;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task13":
				{
					txtField1 = new TextField(115, image1.height, "P(prec)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 98;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(wind)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 58;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task14":
				{
					txtField1 = new TextField(115, image1.height, "P(prec)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 98;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(fog)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 58;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					
					break;
				}
					
				case "task15":
				{
					txtField1 = new TextField(115, image1.height, "P(sun)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 4) + 98;
					txtField1.y = image1.y;
					this.addChild(txtField1);
					
					txtField2 = new TextField(122, image1.height, "P(NOTw)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + (image1.width / 4) - 58;
					txtField2.y = txtField1.y;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task16":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(boat)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(func)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task17":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(close)", "HoboStd", 18, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(NOTuk)", "HoboStd", 18, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task18":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(boat)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(cap)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task19":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(boat)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(tou)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task20":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(NOTuk)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(weat)", "HoboStd", 20, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
					
				case "task21": case "task22":
				{
					txtField1 = new TextField(122, 119, "P(snow)", "HoboStd", 18, Colors.PINK);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + image1.width / 2 + 10;
					txtField1.y = image1.y + image1.height / 2;
					this.addChild(txtField1);
					
					txtField2 = new TextField(50, 119, "P(c|s)", "HoboStd", 10, Colors.VIOLET);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = 560;
					txtField2.y = image1.y + image1.height / 4 - 25;
					this.addChild(txtField2);
					
					txtField3 = new TextField(122, 119, "P(NOTsnow)", "HoboStd", 18, Colors.PINK);
					txtField3.hAlign = HAlign.CENTER;
					txtField3.vAlign = VAlign.CENTER;
					txtField3.x = 425 + 20;
					txtField3.y = image1.y + image1.height / 2;
					this.addChild(txtField3);
					
					txtField4 = new TextField(122, 119, "P(c|NOTs)", "HoboStd", 10, Colors.VIOLET);
					txtField4.hAlign = HAlign.CENTER;
					txtField4.vAlign = VAlign.CENTER;
					txtField4.x = 490;
					txtField4.y = image1.y + image1.height / 4 + 20;
					this.addChild(txtField4);
					
					color = Colors.PINK;
					
					break;
				}
					
				case "task23": case "task24":
				{
					txtField1 = new TextField(122, 119, "P(snow)", "HoboStd", 18, Colors.PINK);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + image1.width / 2 + 10;
					txtField1.y = image1.y + image1.height / 2;
					this.addChild(txtField1);
					
					txtField2 = new TextField(50, 119, "P(l|s)", "HoboStd", 10, Colors.VIOLET);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = 560;
					txtField2.y = image1.y + image1.height / 4 - 25;
					this.addChild(txtField2);
					
					txtField3 = new TextField(122, 119, "P(NOTsnow)", "HoboStd", 18, Colors.PINK);
					txtField3.hAlign = HAlign.CENTER;
					txtField3.vAlign = VAlign.CENTER;
					txtField3.x = 425 + 20;
					txtField3.y = image1.y + image1.height / 2;
					this.addChild(txtField3);
					
					txtField4 = new TextField(122, 119, "P(l|NOTs)", "HoboStd", 10, Colors.VIOLET);
					txtField4.hAlign = HAlign.CENTER;
					txtField4.vAlign = VAlign.CENTER;
					txtField4.x = 490;
					txtField4.y = image1.y + image1.height / 4 + 20;
					this.addChild(txtField4);
					
					color = Colors.PINK;
					
					break;
				}
					
				case "task25":
				{
					txtField1 = new TextField(115, 3 * image1.height / 4, "P(boat)", "HoboStd", 20, Colors.BLUE);
					txtField1.hAlign = HAlign.CENTER;
					txtField1.vAlign = VAlign.CENTER;
					txtField1.x = image1.x + (image1.width / 5) - 25;
					txtField1.y = image1.y + image1.height / 4 + 10;
					this.addChild(txtField1);
					
					txtField2 = new TextField(92, 92, "P(canLeave)", "HoboStd", 14, Colors.PINK);
					txtField2.hAlign = HAlign.CENTER;
					txtField2.vAlign = VAlign.CENTER;
					txtField2.x = image1.x + 122;
					txtField2.y = image1.y + 48;
					this.addChild(txtField2);
					
					color = Colors.YELLOW;
					
					break;
				}
			}
			
			text = "This is the representation of the events space.";
			textField = new TextField(image1.width, 100, text, "HoboStd", 20, color);
			textField.x = image1.x;
			textField.y = image1.y + image1.height + 20;
			textField.hAlign = HAlign.CENTER;
			textField.vAlign = VAlign.TOP;
			this.addChild(textField);
		}
		
		private function setImage(x: int, y: int, width: int, height: int):void
		{
			switch(task)
			{
				case "task01": case "task02": case "task03": case "task04": case "task05":
				{
					image1 = new Image(Assets.getTexture("prompt1"));
					
					break;
				}
					
				case "task06": case "task07": case "task08": case "task09": case "task10":
				{
					image1 = new Image(Assets.getTexture("prompt2"));
					
					break;
				}
					
				case "task11": case "task12": case "task13": case "task14": case "task15":
				{
					image1 = new Image(Assets.getTexture("prompt3"));
					
					break;
				}
					
				case "task16": case "task17": case "task18": case "task19": case "task20": case "task25":
				{
					image1 = new Image(Assets.getTexture("prompt5"));
					
					break;
				}
					
				case "task21": case "task22": case "task23": case "task24": 
				{
					image1 = new Image(Assets.getTexture("prompt6"));
					
					break;
				}
			}
			
			image1.width = width;
			image1.height = height
			image1.x = x;
			image1.y = y;
			this.addChild(image1);
		}
	}
}