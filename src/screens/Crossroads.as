package screens
{
	import flash.utils.Timer;	
	import events.NavigationEvent;
	import objects.Colors;
	import objects.Strings;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import starling.display.Quad;
	import inner_class.InnerClass;
	
	
	public class Crossroads extends Sprite
	{
		private var bckgr: Image;
		private var lQuestion: Button;
		private var rQuestion: Button;
		private var left: Button;
		private var right: Button;
		private var decide: Button;
		private var decideField: TextField;
		private var decideButtonField: TextField;
		private var infoField: TextField;
		private var lField: TextField;
		private var rField: TextField;
		private var ltext: String;
		private var rtext: String;
		private static var timer: Timer;
		public static var choice: String;				// Indica se ho scelto destra o sinistra.
		private static var _prob: int;					// Probabilità in percentuale da confrontare con il numero casuale.
		private var random: int;						// Se random < prob, allora l'ostacolo è stato superato.
		private var addedCost: int;
		private var text: String
		private var bg: Quad;
		private var bg1: Quad;
		private var bg2: Quad;
		private var screenTitle: TextField;
		
		
		public function Crossroads(decision: String)
		{
			super();
			
			choice = decision;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		public static function get prob():int
		{
			return _prob;
		}
		
		public function onAddedToStage(): void
		{		
			bckgr = new Image(Assets.getTexture("bgCrossroads"));
			this.addChild(bckgr);
			
			explaination();
			
			
			if(GameInfo.leftAns)
				
				displayInfo(Strings.LEFT);
				
			else
				
				displayQuestion(Strings.LEFT);
			
			if(GameInfo.rightAns)
				
				displayInfo(Strings.RIGHT);
				
			else
				
				displayQuestion(Strings.RIGHT);
			
			this.addEventListener(Event.TRIGGERED, onButtonClick);
		}	
		
		private function explaination(): void
		{
			var size: int;
			var x: int;
			var y: int;
			var width: int;
			var height: int;
			
			if(!GameInfo.leftAns || !GameInfo.rightAns)
			{
				text = "Solve both problems on the left and on the right.";
				size = 27;
				width = 750;
				height = 85;
				x = 25;
				y = 25;
			}
				
			else
			{
				text = "Which direction do you want to take?";
				size = 24;
				width = 350;
				height = 75;
				x = 220;
				y = 480;
				
				left = new Button(Assets.getTexture("lArrow"));
				left.width = 120;
				left.height = 80;
				//left.x = x - 10;
				//left.y = y + 70;
				left.x = 75;
				left.y = 475;
				this.addChild(left);
				
				right = new Button(Assets.getTexture("rArrow"));
				right.width = 120;
				right.height = 80;
				//right.x = x + 150;
				//right.y = y + 70;
				right.x = 595;
				right.y = 475;
				this.addChild(right);
				
				title();
			}
			
			if(this.contains(bg))
				
				this.removeChild(bg);
			
			AddBg(bg, width, height, x, y, 0xffffff);
			
			infoField = new TextField(width, height, text, "HoboStd", size, Colors.PINK_PEN);
			infoField.x = x;
			infoField.y = y;
			infoField.border = true;
			infoField.hAlign = HAlign.CENTER;
			infoField.vAlign = VAlign.CENTER;
			this.addChild(infoField);
		}
		
		private function title():void
		{
			AddBg(bg, 750, 85, 25, 25, 0xffffff);
			screenTitle = new TextField(750, 85, "Time to make your decision!", "HoboStd", 27, Colors.PINK_PEN);
			screenTitle.x = 25;
			screenTitle.y = 25;
			screenTitle.hAlign = HAlign.CENTER;
			screenTitle.vAlign = VAlign.CENTER;
			screenTitle.border = true;
			this.addChild(screenTitle);
		}
		
		private function changeTitle(): void
		{
			AddBg(bg, 750, 85, 25, 25, 0xffffff);
			this.removeChild(screenTitle);
			screenTitle = new TextField(750, 85, "Oh no, you had to turn back!", "HoboStd", 27, Colors.PINK_PEN);
			screenTitle.x = 25;
			screenTitle.y = 25;
			screenTitle.hAlign = HAlign.CENTER;
			screenTitle.vAlign = VAlign.CENTER;
			screenTitle.border = true;
			this.addChild(screenTitle);
		}
		
		private function changeInfo(): void
		{
			this.removeChild(infoField);
			text = "You can only go ";
			
			if(choice == Strings.RIGHT)
				
				text += "left.";
			
			else
				
				text += "right.";
			
			infoField = new TextField(350, 75, text, "HoboStd", 25, Colors.PINK_PEN);
			infoField.x = 220;
			infoField.y = 480;
			infoField.border = true;
			infoField.hAlign = HAlign.CENTER;
			infoField.vAlign = VAlign.CENTER;
			this.addChild(infoField);
		}
		
		private function AddBg(aBg: Quad, width: int, height: int, x: int, y: int, color: uint): void
		{
			aBg = new Quad(width, height, color, true);
			aBg.x = x;
			aBg.y = y;
			aBg.alpha = 0.5;
			this.addChild(aBg);
		}
		
		private function displayQuestion(direction: String): void
		{
			if(direction == Strings.LEFT)
			{
				lQuestion = new Button(Assets.getTexture("question"));
				lQuestion.width = 100;
				lQuestion.height = 100;
				lQuestion.x = 80;
				lQuestion.y = 250;
				this.addChild(lQuestion);
				
				if(this.contains(lField))
					
					this.removeChild(lField);
				
				ltext = "Click on the question mark to solve this problem!";
				lField = new TextField(150, 130, ltext, "HoboStd", 20, Colors.PEN);
				lField.vAlign = VAlign.CENTER;
				lField.hAlign = HAlign.CENTER;
				lField.x = 60;
				lField.y = lQuestion.y + 110;
				lField.border = true;
				
				if(this.contains(bg1))
					
					this.removeChild(bg1);
				
				AddBg(bg1, 150, 130, lField.x, lField.y, 0xffffff);
				this.addChild(lField);
			}
				
			else
			{
				rQuestion = new Button(Assets.getTexture("question"));
				rQuestion.width = 100;
				rQuestion.height = 100;
				rQuestion.x = 600;
				rQuestion.y = 250;
				this.addChild(rQuestion);
				
				if(this.contains(rField))
					
					this.removeChild(rField);
				
				rtext = "Click the question mark to solve the problem!";
				rField = new TextField(150, 130, rtext, "HoboStd", 20, Colors.PEN);
				rField.vAlign = VAlign.CENTER;
				rField.hAlign = HAlign.CENTER;
				rField.x = 580;
				rField.y = rQuestion.y + 110;
				rField.border = true;
				
				if(this.contains(bg2))
					
					this.removeChild(bg2);
				
				AddBg(bg2, 150, 130, rField.x, rField.y, 0xffffff);
				this.addChild(rField);
			}
		}
		
		private function displayInfo(direction: String):void
		{
			if(direction == Strings.LEFT)
			{
				if(this.contains(lQuestion))
					
					this.removeChild(lQuestion);
				
				ltext = "This way has a probability of\n" + GameInfo.lString + "\nand it takes \n" + GameInfo.lCost + " minutes.";
				lField = new TextField(150, 130, ltext, "HoboStd", 17, Colors.PEN);
				lField.vAlign = VAlign.CENTER;
				lField.hAlign = HAlign.CENTER;
				lField.x = 60;
				lField.y = 300;
				lField.border = true;
				
				if(this.contains(bg1))
					
					this.removeChild(bg1);
				
				AddBg(bg1, 150, 130, lField.x, lField.y, 0xffb2d1);
				this.addChild(lField);
			}
				
			else
			{
				if(this.contains(rQuestion))
					
					this.removeChild(rQuestion);
				
				rtext = "This way has a probability of\n" + GameInfo.rString + "\nand it takes \n" + GameInfo.rCost + " minutes.";
				rField = new TextField(150, 130, rtext, "HoboStd", 17, Colors.PEN);
				rField.vAlign = VAlign.CENTER;
				rField.hAlign = HAlign.CENTER;
				rField.x = 580;
				rField.y = 300;
				rField.border = true;
				
				if(this.contains(bg2))
					
					this.removeChild(bg2);
				
				AddBg(bg2, 150, 130, rField.x, rField.y, 0xffb2d1);
				this.addChild(rField);
			}
		}
		
		private function onButtonClick(event: Event):void
		{
			var buttonClicked: Button = event.target as Button;
			
			
			if((buttonClicked as Button) == lQuestion)
			{
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.LEFT}, true));
			}
				
			else if ((buttonClicked as Button) == rQuestion)
			{
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.RIGHT}, true));
			}
				
			else
			{
				random = Math.floor(Math.random() * 100 + 1);
				
	/*			if(Game.webService)
					
					Game.tuGraz.interact(Strings.COMPETENCES);
				
				else
					
					InnerClass.loadComp();
	*/			
				if((buttonClicked as Button) == left)
				{
					choice = Strings.LEFT;			
					//addedCost = GameInfo.lCost * Game.costPerPoint;
					addedCost = GameInfo.lCost;
					
					if((GameInfo.lType == Strings.YN) && ((GameInfo.lCorrect && GameInfo.leftYN) || (!GameInfo.lCorrect && !GameInfo.leftYN)))
						
						// Task di tipo yes / no. L'utente ha risposto "Yes" e la risposta è corretta ( --> p > 0.5),
						// oppure l'utente ha risposto "No" e la risposta è errata ( --> p > 0.5).
						
						_prob = 75;
						
					else if(GameInfo.lType == Strings.YN)
						
						// Task di tipo yes / no. O l'utente ha risposto "Yes" e la risposta è errata ( --> p < 0.5),
						// o l'utente ha risposto "No" e la risposta è corretta ( --> p < 0.5).
						
						_prob = 25;
						
					else
						
						// Casi di task di tipo yes / no già analizzati.
						
						_prob = GameInfo.lAnswer * 100;
				}
					
				else if ((buttonClicked as Button) == right)
				{
					choice = Strings.RIGHT;
					//addedCost = GameInfo.rCost * Game.costPerPoint;
					addedCost = GameInfo.rCost;
					
					if((GameInfo.lType == Strings.YN) && ((GameInfo.rCorrect && GameInfo.rightYN) || (!GameInfo.rCorrect && !GameInfo.rightYN)))
						
						// Task di tipo yes / no. L'utente ha risposto "Yes" e la risposta è corretta ( --> p > 0.5),
						// oppure l'utente ha risposto "No" e la risposta è errata ( --> p > 0.5).
						
						_prob = 75;
						
					else if(GameInfo.rType == Strings.YN)
						
						// Task di tipo yes / no. O l'utente ha risposto "Yes" e la risposta è errata ( --> p < 0.5),
						// o l'utente ha risposto "No" e la risposta è corretta ( --> p < 0.5).
						
						_prob = 25;
						
					else
						
						// Casi di task di tipo yes / no già analizzati.
						
						_prob = GameInfo.rAnswer * 100;
				}
				
				// Ho settato il valore di prob in tutti i casi possibili. Ora lo comparo con random.
				
				if(random < _prob)
				{
					// L'utente ha superato l'ostacolo, quindi va avanti.
					Game.time += addedCost;
					
					this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.SUBMIT}, true));
				}
				
				else
				{
					// L'utente non ha superato l'ostacolo, quindi deve tornare indietro (spende il doppio del tempo). 
					
					Game.time += 2 * addedCost;
					
					this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.FAILED}, true));
				}
			}
		}
		
		public function turnBack(): void
		{
			if(choice == Strings.RIGHT)
			{
				this.removeChild(right);
				this.removeChild(rField);
				
				rtext = "You did not manage to overcome this path.";
				rField = new TextField(150, 130, rtext, "HoboStd", 17, Colors.PEN);
				rField.vAlign = VAlign.CENTER;
				rField.hAlign = HAlign.CENTER;
				rField.x = 580;
				rField.y = 300;
				rField.border = true;
				this.addChild(rField);
			}
				
			else if(choice == Strings.LEFT)
			{
				this.removeChild(left);
				this.removeChild(lField);
				
				ltext = "You did not manage to overcome this path.";
				lField = new TextField(150, 130, ltext, "HoboStd", 17, Colors.PEN);
				lField.vAlign = VAlign.CENTER;
				lField.hAlign = HAlign.CENTER;
				lField.x = 60;
				lField.y = 300;
				lField.border = true;
				this.addChild(lField);
			}
			
			this.changeTitle();
			this.changeInfo();
			this.addEventListener(Event.TRIGGERED, secondChoice);
		}
		
		private function secondChoice():void
		{
			if(Game.closed)
				
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.CLOSED}, true));
				
			else
				
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.CONT_PLAY}, true));
			//this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.SECOND_CHOICE}, true));
		}
	}
}