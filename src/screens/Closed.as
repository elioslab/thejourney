package screens
{
	import events.NavigationEvent;
	
	import objects.Strings;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	
	public class Closed extends Sprite
	{
		private var bg: Image;
		private var back: Button;
		private var _direction: String;
		
		
		public function Closed(choice: String)
		{
			super();
			
			_direction = choice;
			bg = new Image(Assets.getTexture("closed"));
			this.addChild(bg);
			back = new Button(Assets.getAtlas().getTexture("backButton"));
			back.width = 150;
			back.height = 150;
			back.x = 640;
			back.y = 440;
			this.addChild(back);
			
			this.addEventListener(Event.TRIGGERED, onClick);
		}
		
		public function get direction():String
		{
			return _direction;
		}
		
		private function onClick():void
		{
			this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.BACK_CROSSROADS}, true));
		}
	}
}