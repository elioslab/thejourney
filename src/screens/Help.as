package screens
{
	import events.NavigationEvent;
	import objects.Colors;
	import objects.Strings;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	
	
	public class Help extends Sprite
	{
		private var bg: Image;
		private var back: Button;
		private var textField: TextField;
		private var formula: String;
		private var textField2:TextField;
		private var formula2: String;
		
		
		public function Help()
		{
			super();
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			bg = new Image(Assets.getTexture("bgNotes"));
			this.addChild(bg);
			
			back = new Button(Assets.getAtlas().getTexture("backButton"));
			back.x = 630;
			back.y = 430;
			this.addChild(back);
			
			formula = "Probability space:\n\nP(A) = Na / Ntot\tNtot tends to infinity\n0 <= P(A) <= 1\n";
			formula += "P(A) = 0\t Impossible event\nP(A) = 1\tCertain event\n";
			formula += "P(not A) = 1 – P(A)\n\n\nProbability of disjoint (or mutually exclusive) events:\n\nP(A, B) = 0\nP(A + B) = P(A) + P(B)\n";
			
			textField = new TextField(250, 450, formula, "HoboStd", 18, Colors.PEN);
			textField.x = 90;
			textField.y = 20;
			this.addChild(textField);
			
			formula2 = "Probability of non mutually exclusive events:\n\nP(A, B) = 0\tis FALSE\nP(A + B) = P(A) + P(B) – P(A, B)\n";
			formula2 += "\n\nProbability of independent events:\n\nP(A, B) = P(A) * P(B)\n\n\nProbability of dependent events:\nPr(A|B) = Pr(A, B)/Pr(B)";
			
			textField2 = new TextField(300, 450, formula2, "HoboStd", 18, Colors.PEN);
			textField2.x = 410;
			textField2.y = 10;
			this.addChild(textField2);
			
			this.addEventListener(Event.TRIGGERED, onHelpClick);
		}
		
		private function onHelpClick():void
		{
			this.removeEventListener(Event.TRIGGERED, onHelpClick);
			this.visible = false;
			this.removeFromParent();
			this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.TASK}, true));
		}
	}
}