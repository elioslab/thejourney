package screens
{
	import flash.text.TextField;
	
	import events.NavigationEvent;
	
	import objects.Colors;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	//import com.greensock.TweenLite;
	
	public class Welcome extends Sprite
	{
		private var bckground: Image;
		private var title: Image;
		private var play: Button;
		private var about: Button;
		
		private var textField: TextField;
		
		
		public function Welcome()
		{
			super();
			
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(event: Event):void
		{
			trace("Welcome screen inizialized!");
			
			drawScreen();
		}
		
		private function drawScreen():void
		{
			bckground = new Image(Assets.getTexture("bgWelcome"));
			this.addChild(bckground);
			
			title = new Image(Assets.getAtlas().getTexture("title"));
			title.x = 5;
			title.y = 100;
			this.addChild(title);
			
			play = new Button(Assets.getAtlas().getTexture("playButton"));
			play.x = 600;
			play.y = 395;
			this.addChild(play);
			
			about = new Button(Assets.getAtlas().getTexture("aboutButton"));
			about.x = 20;
			about.y = 265;
			this.addChild(about);
			
			this.addEventListener(Event.TRIGGERED, onMainMenuClick);
		}
		
		public function inizialize(): void
		{
			this.visible = true;
			
			//TweenLite.to(person, 2, {x: 80});
			
			this.addEventListener(Event.ENTER_FRAME, animations)
		}
		
		private function animations():void
		{
			var date: Date = new Date();
			play.y = 400 + (Math.cos(date.getTime() * 0.002) * 10);
			about.y = 420 + (Math.cos(date.getTime() * 0.002) * 10);
			title.x = 5 + (Math.sin(date.getTime() * 0.002) * 4);
			title.y = 100 + (Math.cos(date.getTime() * 0.002) * 5);
		}
		
		private function onMainMenuClick(event: Event):void
		{
			var buttonClicked: Button = event.target as Button;
			
			if((buttonClicked as Button) == play)
			{
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: "play"}, true));
			}
				
			else
			{
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: "about"}, true));
			}
		}
		
		public function disposeTemporarily():void
		{
			this.visible = false;
			
			if(this.hasEventListener(Event.ENTER_FRAME))
				
				this.removeEventListener(Event.ENTER_FRAME, animations)
		}
	}
}