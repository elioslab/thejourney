package screens
{
	import events.NavigationEvent;
	import objects.Colors;
	import objects.Strings;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	public class SecondChoice extends Sprite
	{
		private var bg: Image;
		private var girl: Image;
		private var boy: Image;
		private var bubble: Image;
		private var go: Button;
		private var screen: String;
		private var text: String;
		private var textField: TextField;
		private var tFHeight: int;
		private var tFWidth: int;
		private var tFDimension: int;
		private var scoreField: TextField;
		
		
		public function SecondChoice(aScreen: String)
		{
			super();
			
			screen = aScreen;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			go = new Button(Assets.getTexture("contButton"));
			text = "I am so sorry that you had lost your time!";
			
			switch(screen)
			{
				case Strings.MOUNTAINS:
				{
					bg = new Image(Assets.getTexture("bgMountains"));
					this.addChild(bg);
					boy = new Image(Assets.getTexture("boy2"));
					boy.width = 400;
					boy.height = 902;
					boy.x = 225;
					boy.y = 100;
					this.addChild(boy);
					bubble = new Image(Assets.getTexture("bubble1"));
					bubble.width = 300;
					bubble.height = 300;
					bubble.x = -40;
					bubble.y = 350;
					this.addChild(bubble);
					textField = new TextField(200, 150, text, "HoboStd", 30, Colors.PEN);
					textField.hAlign = HAlign.CENTER;
					textField.vAlign = VAlign.CENTER;
					textField.x = 15;
					textField.y = 440;
					this.addChild(textField);
					go.x = 600;
					go.y = 395;
					scoreField = new TextField(350, 50, "Score: " + Game.score, "HoboStd", 40, 0xA6238D);
					scoreField.hAlign = HAlign.LEFT;
					scoreField.vAlign = VAlign.TOP;
					scoreField.x = 10;
					scoreField.y = 0;
					this.addChild(scoreField);
					
					break;
				}
					
				case Strings.LAKE:
				{
					bg = new Image(Assets.getTexture("bgLake"));
					this.addChild(bg);
					girl = new Image(Assets.getTexture("girl1"));
					girl.width = 427;
					girl.height = 800;
					girl.x = 400;
					girl.y = 100;
					this.addChild(girl);
					bubble = new Image(Assets.getTexture("bubble2"));
					bubble.width = 400;
					bubble.height = 300;
					bubble.x = 20;
					bubble.y = 20;
					this.addChild(bubble);
					textField = new TextField(300, 200, text, "HoboStd", 30, Colors.PEN);
					textField.hAlign = HAlign.CENTER;
					textField.vAlign = VAlign.CENTER;
					textField.x = 60;
					textField.y = 50;
					this.addChild(textField);
					go.height = go.width = 150;
					go.x = 20;
					go.y = 430;
					scoreField = new TextField(350, 50, "Score: " + Game.score, "HoboStd", 40, 0xEB1168);
					scoreField.hAlign = HAlign.LEFT;
					scoreField.vAlign = VAlign.TOP;
					scoreField.x = 550;
					scoreField.y = 0;
					this.addChild(scoreField);
					
					break;
				}
					
				case Strings.RIVER:
				{
					bg = new Image(Assets.getTexture("bgRiver"));
					this.addChild(bg);
					bubble = new Image(Assets.getTexture("bubble1"));
					bubble.rotation = Math.PI;
					bubble.width = 400;
					bubble.height = 300;
					bubble.x = 650;
					bubble.y = 350;
					this.addChild(bubble);
					textField = new TextField(300, 200, text, "HoboStd", 30, Colors.PEN);
					textField.hAlign = HAlign.CENTER;
					textField.vAlign = VAlign.CENTER;
					textField.x = 315;
					textField.y = 80;
					this.addChild(textField);
					go.x = 600;
					go.y = 395;
					scoreField = new TextField(350, 50, "Score: " + Game.score, "HoboStd", 40, 0x242670);
					scoreField.hAlign = HAlign.LEFT;
					scoreField.vAlign = VAlign.TOP;
					scoreField.x = 10;
					scoreField.y = 0;
					
					break;
				}
			}
			
			textField.hAlign = HAlign.CENTER;
			textField.vAlign = VAlign.CENTER;
			
			this.addChild(textField);
			this.addChild(go);
			this.addChild(scoreField);
			
			this.addEventListener(Event.TRIGGERED, changeScreen);
		}
		
		private function changeScreen():void
		{
			this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.CONT_PLAY}, true));
		}
	}
}