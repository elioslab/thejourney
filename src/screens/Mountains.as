package screens
{
	import events.NavigationEvent;
	
	import objects.Strings;
	import objects.Colors;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	
	
	public class Mountains extends Sprite
	{
		private var background: Image;
		private var boy: Image;
		private var bubble: Image;
		private var go: Button;
		private var error: Button;
		private var first: Boolean;
		private var text: String;
		private var cost: int;
		private var textField: TextField;
		private var scoreField: TextField;
		
		
		public function Mountains()
		{
			super();
			
			first = true;
			
			GameInfo.setCost();
			
			if(GameInfo.right)
				
				cost = GameInfo.rCost;
				
			else
				
				cost = GameInfo.lCost;
			
			this.addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			background = new Image(Assets.getTexture("bgMountains"));
			this.addChild(background);
			
			boy = new Image(Assets.getTexture("boy2"));
			boy.width = 400;
			boy.height = 902;
			boy.x = 225;
			boy.y = 100;
			this.addChild(boy);
			
			bubble = new Image(Assets.getTexture("bubble1"));
			bubble.width = 300;
			bubble.height = 300;
			bubble.x = -40;
			bubble.y = 350;
			this.addChild(bubble);
			
			go= new Button(Assets.getTexture("contButton"));
			go.x = 600;
			go.y = 395;
			this.addChild(go);
			
			text = "This path will take " + cost + " minutes!";
			textField = new TextField(200, 150, text, "HoboStd", 25, Colors.PEN);
			textField.hAlign = HAlign.CENTER;
			textField.vAlign = VAlign.CENTER;
			textField.x = 15;
			textField.y = 440;
			this.addChild(textField);
			
			displayScore();
			
			this.addEventListener(Event.TRIGGERED, onClick);
		}
		
		private function insertErrorButton(width: int, height: int, x: int, y: int): void
		{
			if(this.contains(error))//;
				this.removeChild(error);
			
			error = new Button(Assets.getTexture("error"));
			error.width = width;
			error.height = height;
			error.x = x;
			error.y = y;
			this.addChild(error);
		}
		
		private function displayScore():void
		{
			if(this.contains(scoreField))
				
				this.removeChild(scoreField);
			
			scoreField = new TextField(350, 50, "Minutes: " + Game.time, "HoboStd", 40, 0xA6238D);
			scoreField.hAlign = HAlign.LEFT;
			scoreField.vAlign = VAlign.TOP;
			scoreField.x = 40;
			scoreField.y = 0;
			this.addChild(scoreField);
		}
		
		private function onClick(event: Event):void
		{	
			var buttonClicked: Button = event.target as Button;
			
			if((buttonClicked as Button) == error)
				
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.ERROR}, true));
				
			else if(first)
				
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.GO_MOUNTAINS}, true));
				
			else
				
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.CONT_PLAY}, true));
			
			first = false;
		}
		
		public function submitScreen():void
		{
			/*this.removeChild(background);
			background = new Image(Assets.getTexture("bgMountain2"));
			this.addChild(background);
			this.removeChild(go);
			go.width = 150;
			go.height = 150;
			go.x += 20;
			go.y += 20;
			this.addChild(go);*/
			
			this.addEventListener(Event.TRIGGERED, onClick);
			
			/*if((Crossroads.prob <= 0.5) && GameInfo.rCorrect && GameInfo.lCorrect)
			{
			// L'utente ha superato l'ostacolo anche se la probabilità di riuscita era bassa e, inoltre, ha risposto correttamente ad entrambi i task.
			
			text = "Congratulations, you have overcomed the path. You have been really lucky!";
			}
			
			else if((Crossroads.prob > 0.5) && GameInfo.rCorrect && GameInfo.lCorrect)
			{
			// L'utente ha superato l'ostacolo e, inoltre, ha risposto correttamente ad entrambi i task.
			
			text = "Congratulations, you have overcomed the path.";
			}
			
			else if(Crossroads.prob > 0.5)
			{
			// L'utente ha superato l'ostacolo, ma ha fornito almeno una risposta sbagliata. Necessità di spiegazioni.
			
			text = "You have overcomed the path, but maybe it is just because you are lucky!";
			
			insertErrorButton(80, 80, 700, 20);
			}*/
			
			text = Strings.SUB_NO_MISTAKES;
			this.removeChild(textField);
			textField = new TextField(200, 150, text, "HoboStd", 18, Colors.PEN);
			textField.hAlign = HAlign.CENTER;
			textField.vAlign = VAlign.CENTER;
			textField.x = 10;
			textField.y = 440;
			this.addChild(textField);
			
			//this.removeEventListener(Event.TRIGGERED, onClick);
			//this.addEventListener(Event.TRIGGERED, GoBack);
			
			displayScore();
		}
		
		public function failedScreen(): void
		{
			if(GameInfo.lCorrect && GameInfo.rCorrect)
			{
				// L'utente non ha superato l'ostacolo, ma ha fornito le risposte corrette ad entrambi i task.
				
				text = Strings.FAIL_NO_MISTAKES;
				displayErr = false; 
			}
				
			else
			{	
				// L'utente non ha superato l'ostacolo e, inoltre, ha fornito almeno una risposta errata. Necessità di spiegazioni.
				
				text = Strings.FAIL_MISTAKES;
				var displayErr:Boolean = true;
			}
			
			this.removeChild(bubble);
			bubble.width += 30;
			this.addChild(bubble);
			this.removeChild(textField);
			textField = new TextField(220, 210, text, "HoboStd", 16, Colors.PEN);
			textField.hAlign = HAlign.CENTER;
			textField.vAlign = VAlign.CENTER;
			textField.x = 5;
			textField.y = 410;
			this.addChild(textField);
			
			if(displayErr)
				
				insertErrorButton(100, 40, 65, 515);
			
			this.removeEventListener(Event.TRIGGERED, onClick);
			this.addEventListener(Event.TRIGGERED, GoBack);
			
			displayScore();
		}
		
		private function GoBack(event: Event):void
		{
			var buttonClicked: Button = event.target as Button;
			
			if((buttonClicked as Button) == error)
				
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.ERROR}, true));
				
			else
				//this.visible = false;
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id: Strings.BACK_CROSSROADS}, true));
		}
	}
}