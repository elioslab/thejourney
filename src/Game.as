package
{
	import flash.events.TimerEvent;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	import events.NavigationEvent;
	import events.ReadyEvent;
	
	import inner_class.InnerClass;
	
	import objects.Colors;
	import objects.Strings;
	
	import screens.About;
	import screens.Closed;
	import screens.Correction;
	import screens.Crossroads;
	import screens.Help;
	import screens.HowTo;
	import screens.InGame;
	import screens.Lake;
	import screens.Mountains;
	import screens.Notes;
	import screens.Prompt;
	import screens.River;
	import screens.SecondChoice;
	import screens.Solved;
	import screens.TheEnd;
	import screens.Welcome;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import objects.Exit;
	
	
	public class Game extends Sprite
		
	{
		private var screenWelcome: Welcome;
		private var screenHowTo: HowTo;
		private var screenInGame: InGame;
		private var screenAbout: About;
		private var screenSolved: Solved;
		private var screenRiver: River;
		private var screenLake: Lake;
		private var screenMountains: Mountains;
		private var screenNotes: Notes;
		private var screenHelp: Help;
		private var screenCrossroads: Crossroads;
		private var screenSecondChoice: SecondChoice;
		private var screenPrompt: Prompt;
		private var screenCorrection: Correction;
		private var screenEnd: TheEnd;
		private var screenClosed: Closed;
		private var lScreen: String;						// Indica il luogo del task a sinistra.
		private var rScreen: String;						// Indica il luogo del task a destra.
		private var first: Boolean;
		public static var submit: Boolean;
		public static var score: int;
		public static var time: int;
		private static var _wrongAns: int;
		private static var _costPerPoint: int;
		private static var _costPerPrompt: int;
		private static var _timePrompt: int;				// Tempo in millisecondi che devono passare prima di mostrare il prompt.
		private static var _timeWalking: int;				// Tempo in millisecondi di camminata.
		private static var _nCompetences: int;
		private static var _tollerance: Number;
		public var screen: String;
		public static var database: Database;
		public static var tuGraz: TuGraz;
		private static var _next: Boolean;
		private static var _committed: Boolean;
		private static var _loading: Image;
		private static var bg: Image;
		private static var _closed: Boolean;
		private static var _webService: Boolean;
		private var innerClass: InnerClass;
		private var exit: Button;
		private var exitWndw: Exit;
		
		
		private static var timer: Timer;
		
		
		public function Game()
		{
			super();
			
			score = 0;							// Score riferito alle effettive conoscenze.
			time = 0;							// Score visualizzato dall'utente.
			_closed = false;
			_wrongAns = 100;
			_costPerPoint = 50;
			_costPerPrompt = 50;
			_timePrompt = 2 * 60 * 1000;
			_timeWalking = 3000;
			_nCompetences = 0;
			_tollerance = 0.05;
			_next = false;
			_loading = new Image(Assets.getTexture("loading"));
			first = true;			
			bg = _loading;
			this.addChild(bg);
			
			_webService = true;
			
			database = new Database();
			database.addEventListener(events.ReadyEvent.DB_CONNECTED, onDbConnected);
			database.ConnectDb("database.db");
			
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
			var html: HTML = new HTML(Strings.HELP_REF, 5, 5);
			this.addChild(html);
		}		
		
		public static function get webService():Boolean
		{
			return _webService;
		}

		public static function get tollerance():Number
		{
			return _tollerance;
		}
		
		public static function get closed():Boolean
		{
			return _closed;
		}
		
		public static function get loading():Image
		{
			return _loading;
		}
		
		public static function get committed():Boolean
		{
			return _committed;
		}
		
		public static function get timeWalking():int
		{
			return _timeWalking;
		}
		
		public static function get next():Boolean
		{
			return _next;
		}
		
		private function onDbConnected(event: ReadyEvent):void
		{
			database.removeEventListener(ReadyEvent.DB_CONNECTED, onDbConnected);
			
			if(webService)
			{
				database.addEventListener(ReadyEvent.SVC_INIT, onSvcInit);
				
				tuGraz = new TuGraz();
				tuGraz.interact(Strings.INIT);
			}
			
			else
			{	
				innerClass = new InnerClass();
				onSvcInit();
			}
		}
		
		private function onSvcInit():void
		{
			if(webService)
			
				database.removeEventListener(ReadyEvent.SVC_INIT, onSvcInit);
			
			inizialize();
			screenWelcome = new Welcome();
			this.addChild(screenWelcome);
			screenWelcome.inizialize();
			insertExitBtn(680, 10);
		}
		
		public static function get timePrompt():int
		{
			return _timePrompt;
		}
		
		public static function get costPerPrompt():int
		{
			return _costPerPrompt;
		}
		
		public static function get costPerPoint():int
		{
			return _costPerPoint;
		}
		
		public static function get wrongAns():int
		{
			return _wrongAns;
		}
		
		private function onAddedToStage(event:Event):void
		{
			this.addEventListener(events.NavigationEvent.CHANGE_SCREEN, onChangeScreen);
		}
		
		private function inizialize():void
		{
			database.ConnectDb("database.db");
			
			if(webService)
				
				database.addEventListener(ReadyEvent.SVC_NEXT1, onNextTask1);
			
			_next = false;
			_committed = false;
			
			bg = _loading;
			this.addChild(bg);
			
			screenHowTo = new HowTo();
			screenHowTo.visible = false;
			this.addChild(screenHowTo);
			
			screenInGame = new InGame();
			screenInGame.disposeTemporarily();
			this.addChild(screenInGame);
			
			screenAbout = new About();
			screenAbout.visible = false;
			this.addChild(screenAbout);
		}
		
		private function insertExitBtn(x: int, y: int): void
		{
			if(this.contains(exit))
				
				this.removeChild(exit);
			
			exit = new Button(Assets.getTexture("exit"));
			exit.width = 100;
			exit.height = 110;
			exit.x = x;
			exit.y = y;
			this.addEventListener(Event.TRIGGERED, onExit);
			this.addChild(exit);
		}
		
		private function onExit(event: Event):void
		{
			var buttonClicked: Button = event.target as Button;
			
			if((buttonClicked as Button) == exit)
			{
				exitWndw = new Exit();
				this.addChild(exitWndw);
				insertExitBtn(695, 505);
			}
		}
		
		private function onNextTask1():void
		{
			database.removeEventListener(ReadyEvent.SVC_NEXT1, onNextTask1);
			_next = true;
		}
		
		private function onChangeScreen(event: NavigationEvent):void
		{
			switch(event.params.id)
			{
				case Strings.ABOUT:
				{	
					screenWelcome.visible = false;
					screenAbout.inizialize();
					insertExitBtn(10, 500);
					
					break;
				}
					
				case Strings.BACK:
				{	
					screenAbout.visible = false;
					screenWelcome.inizialize();
					removeChild(exit);
					insertExitBtn(680, 10);
					
					break;
				}
					
				case Strings.BACK_CORRECTION:
				{
					this.removeChild(screenCorrection);
					
					if(Crossroads.choice == Strings.RIGHT)
						
						screen = rScreen;
						
					else
						
						screen = lScreen;
					
					if(screen ==  Strings.LAKE && submit)
					{
						this.removeChild(screenLake);						
						screenLake = new Lake();
						screenLake.visible = false;
						this.addChild(screenLake);
						screenLake.submitScreen();
						screenLake.visible = true;
					}
						
					else if(screen ==  Strings.LAKE && !submit)
					{
						screenLake.visible = true;
						screenLake.failedScreen();
					}
						
					else if(screen == Strings.MOUNTAINS && submit)
					{
						screenMountains.visible = true;
						screenMountains.submitScreen();
					}
						
					else if(screen == Strings.MOUNTAINS && !submit)
					{
						screenMountains.visible = true;
						screenMountains.failedScreen();
					}
						
					else if(submit)
					{
						screenRiver = new River();
						screenRiver.submitScreen();
						this.addChild(screenRiver);
					}
						
					else
					{
						screenRiver = new River();
						screenRiver.visible = true;
						screenRiver.failedScreen();
						this.addChild(screenRiver);
					}											
				}
					
				case Strings.BACK_CROSSROADS:
				{
					if(this.contains(screenClosed))
					{
						screenCrossroads = new Crossroads(screenClosed.direction);
						this.removeChild(screenClosed);
						/*	inputField = new TextField(800, 600, "BACK CROSSROADS!!", "HoboStd", 30, Colors.PINK_PEN);
						inputField.border = true;
						inputField.hAlign = HAlign.CENTER;
						inputField.vAlign = VAlign.CENTER;
						this.addChild(inputField);	*/
						screenCrossroads.turnBack();
						this.addChild(screenCrossroads);
					}
						
					else
					{
						if(Crossroads.choice == Strings.RIGHT)
						{
							switch(rScreen)
							{
								case Strings.MOUNTAINS:
								{
									this.removeChild(screenMountains);
									
									break;
								}
									
								case Strings.LAKE:
								{
									this.removeChild(screenLake);
									
									break;
								}
									
								case Strings.RIVER:
								{
									this.removeChild(screenRiver);
									
									break;
								}
							}
						}
							
						else
						{
							switch(lScreen)
							{
								case Strings.MOUNTAINS:
								{
									this.removeChild(screenMountains);
									
									break;
								}
									
								case Strings.LAKE:
								{
									this.removeChild(screenLake);
									
									break;
								}
									
								case Strings.RIVER:
								{
									this.removeChild(screenRiver);
									
									break;
								}
							}
						}
						
						screenCrossroads.turnBack();
						screenCrossroads.visible = true;
					}
					
					insertExitBtn(695, 505);
					
					break;
				}
					
				case Strings.BACK_HOME:
				{
					this.removeChildren();
					onSvcInit();
					
					break;
				}
				
				case Strings.BACK_PROMPT:
				{
				//	this.removeChild(screenPrompt);
					
					if(this.contains(screenPrompt))
					{	
						this.removeChild(screenPrompt);
						screenPrompt = null;			
					}
					
					screenNotes.resetButtons();
					screenNotes.alpha = 1;
					
					break;
				}
					
				case Strings.CLOSED:
				{
					endGame();
					
					break;
				}
					
				case Strings.CONT_PLAY:
				{
					/*if(this.contains(screenSecondChoice))
						
						this.removeChild(screenSecondChoice);
					
					if(this.contains(screenHelp))
					{
						screenHelp.visible = false;
						this.removeChild(screenHelp);
					}*/
					
					trace("\nCONT PLAY\n");
					
					if(this.contains(screenCrossroads))
						
						this.removeChild(screenCrossroads);
					
					if(this.contains(screenHelp))
					{
						screenHelp.visible = false;
						this.removeChild(screenHelp);
					}
					
					
					// Devo verificare ci sono ancora competenze NON acquisite dall'utente.
					
					if(webService)
					{
						database.addEventListener(ReadyEvent.SVC_COMP, onCompetences);
						tuGraz.interact(Strings.COMPETENCES);
					}
					
					else
					{
						InnerClass.loadNextTask();
						onCompetences();
					}
					
					insertExitBtn(680, 10);
				}
					
				case Strings.CONTINUE:
				{	
					screenHowTo.update();
					
					break;
				}
					
				case Strings.CROSSROADS:
				{
					if((webService && TuGraz.nextTask == null) || (!webService && InnerClass.nextTask == null))
					{
						endGame();
					}
						
					else
					{
						if(GameInfo.leftAns || GameInfo.rightAns)
						{
							// Se sono qui è perchè l'utente ha già risposto al primo task. Devo quindi eseguire la COMMIT e cercare il nuovo task.
							
							commit();
						}
						
						this.removeChild(screenNotes);
						//screenSolved.visible = false;
						screenCrossroads = new Crossroads("");
						screenCrossroads.visible = true;
						this.addChild(screenCrossroads);
						insertExitBtn(695, 505);
					}
					
					break;
				}
					
				case Strings.ERROR:
				{
					screenCorrection = new Correction();
					screenCorrection.visible = true;
					this.addChild(screenCorrection);
					
					break;
				}
					
				case Strings.FAILED:
				{
					screen: String;
					
					// Devo eseguire la COMMIT per il secondo task.
					
					commit();
					
					if(Crossroads.choice == Strings.LEFT)
						
						screen = lScreen;
						
					else
						
						screen = rScreen;
					
					//this.removeChild(screenCrossroads);
					screenCrossroads.visible = false;
					
					switch(screen)
					{
						case Strings.MOUNTAINS:
						{
							screenMountains.failedScreen();
							screenMountains.visible = true;
							insertExitBtn(680, 10);
							
							break;
						}
							
						case Strings.LAKE:
						{
							screenLake.failedScreen();
							screenLake.visible = true;
							insertExitBtn(695, 505);
							
							break;
						}
							
						case Strings.RIVER:
						{
							screenRiver.failedScreen();
							screenRiver.visible = true;
							insertExitBtn(680, 10);
							
							break;
						}
					}
					
					submit = false;
					
					break;
				}	
					
				case Strings.GO_LAKE:
				{
					var nextTask: String;
					
					if(webService)
						
						nextTask = TuGraz.nextTask;
					
					else
						
						nextTask = InnerClass.nextTask;
					
					screenNotes = new Notes(GameInfo.screen, nextTask);
					this.addChild(screenNotes);
					screenLake.visible = false;
					screenNotes.visible = true;
					
					break;
				}
					
				case Strings.GO_MOUNTAINS:
				{
					if(webService)
						
						nextTask = TuGraz.nextTask;
						
					else
						
						nextTask = InnerClass.nextTask;
					
					screenNotes = new Notes(GameInfo.screen, nextTask);
					this.addChild(screenNotes);
					screenMountains.visible = false;
					screenNotes.visible = true;
					
					break;
				}
					
				case Strings.GO_RIVER:
				{	
					if(webService)
						
						nextTask = TuGraz.nextTask;
						
					else
						
						nextTask = InnerClass.nextTask;
					
					screenNotes = new Notes(GameInfo.screen, nextTask);
					this.addChild(screenNotes);
					screenRiver.visible = false;
					screenNotes.visible = true;
					
					break;
				}
					
				case Strings.HELP:
				{
					screenHelp = new Help();
					screenHelp.visible = true;
					this.addChild(screenHelp);
					//screenNotes.visible = false;
					//screenHelp.visible = true;
					
					break;
				}
					
				case Strings.LEFT:
				{	
					screenInGame.visible = false;
					this.removeChild(screenCrossroads);
					inizializeScreen("left");
					
					break;
				}
					
				case Strings.LETSGO:
				{	
					screenHowTo.visible = false;
					this.removeChild(screenWelcome);
					screenInGame.inizialize();
					
					break;
				}
					
				case Strings.PLAY:
				{	
					screenWelcome.disposeTemporarily();
					screenHowTo.inizialize();
					insertExitBtn(680, 10);
					
					break;
				}
					
				case Strings.PLAY_AGAIN:
				{
					this.removeChildren();
					
					score = 0;
					time = 0;
					_wrongAns = 100;
					_costPerPoint = 50;
					_costPerPrompt = 50;
					_timePrompt = 3 * 60000;
					
					_nCompetences = 0;
					
					first = true;
					
					this.addEventListener(events.NavigationEvent.CHANGE_SCREEN, onChangeScreen);
					inizialize();
				}
					
				case Strings.PROMPT:
				{
					//screenNotes.visible = false;	
					
					screenPrompt = new Prompt();
					screenPrompt.visible = true;
					this.addChild(screenPrompt);
					
					break;
				}
					
				case Strings.RIGHT:
				{
					screenInGame.visible = false;
					this.removeChild(screenCrossroads);
					inizializeScreen("right");
					
					break;	
				}	
					
				case Strings.SECOND_CHOICE:
				{
					this.removeChild(screenCrossroads);
					
					if(Crossroads.choice == Strings.RIGHT)
						
						screenSecondChoice = new SecondChoice(lScreen);
						
					else
						
						screenSecondChoice = new SecondChoice(rScreen);
					
					screenSecondChoice.visible = true;
					this.addChild(screenSecondChoice);
					
					break;
				}
					
				case Strings.SKIP:
				{	
					screenHowTo.visible = false;
					this.removeChild(screenWelcome);
					screenInGame.inizialize();
					
					break;
				}
					
				case Strings.SUBMIT:
				{
					// Devo eseguire la COMMIT per il secondo task.
					
					commit();
					
					if(Crossroads.choice == Strings.LEFT)
						
						screen = lScreen;
						
					else
						
						screen = rScreen;
					
					this.removeChild(screenCrossroads);
					
					
					switch(screen)
					{
						case Strings.MOUNTAINS:
						{
							screenMountains.submitScreen();
							screenMountains.visible = true;
							insertExitBtn(680, 10);
							
							break;
						}
							
						case Strings.LAKE:
						{
							screenLake.submitScreen();
							screenLake.visible = true;
							insertExitBtn(695, 505);
							
							break;
						}
							
						case Strings.RIVER:
						{
							screenRiver.submitScreen();
							screenRiver.visible = true;
							insertExitBtn(680, 10);
							
							break;
						}
					}
					
					submit = true;
					
					break;
				}
					
				case Strings.TASK:
				{
					screenHelp.visible = false;
					this.removeChild(screenHelp);
					//screenHelp.visible = false;
					screenNotes.visible = true;
					insertExitBtn(680, 500);
					
					break;
				}
			}
		}
		
		private function onCompetences():void
		{
			trace("\nON COMPETENCE\n");
			
			if((webService && TuGraz.nCompetences >= 5) || (!webService && InnerClass.nAquiredComp == 5))
			{
				trace("Game end");
				endGame();
			}
				
			else
			{
				this.removeChildren();
				inizialize();
				screenInGame = new InGame();
				this.addChild(screenInGame);
				screenInGame.drawGame();
				screenInGame.inizialize();
			}
		}
		
		private function endGame():void
		{
			if(webService)
			{
				database.addEventListener(ReadyEvent.SVC_STOP, onStop);
				tuGraz.interact(Strings.STOP);
			}
			
			this.removeChildren();
			screenEnd = new TheEnd();
			screenEnd.visible = true;
			this.addChild(screenEnd);
			insertExitBtn(690, 500);
		}
		
		private function onStop():void
		{
			tuGraz.interact(Strings.DELETE);
		}
		
		private function commit():void
		{
			_committed = false;
			_next = false;
			
			if(webService)
			{
				database.addEventListener(ReadyEvent.SVC_COMMITTED, onCommitted);
				tuGraz.interact(Strings.COMMIT);
			}
			
			else
			{
				InnerClass.commit();
				onCommitted();
			}
		}
		
		private function onCommitted():void
		{
			_committed = true;
			
			if(webService)
				
				database.removeEventListener(ReadyEvent.SVC_COMMITTED, onCommitted);
			
			if((!GameInfo.leftAns && GameInfo.rightAns) || (GameInfo.leftAns && !GameInfo.rightAns))
			{
				// Se sono qui è perchè l'utente ha risposto solo ad un task.
				
				if(webService)
				{
					database.addEventListener(ReadyEvent.SVC_NEXT2, onNextTask2);
					tuGraz.interact(Strings.NEXT_TASK);
				}
				
				else
				{
					InnerClass.loadNextTask();
					_next = true;
				}
			}
		}
		
		private function onNextTask2():void
		{
			database.removeEventListener(ReadyEvent.SVC_NEXT2, onNextTask2);
			_next = true;
		}
		
		private function inizializeScreen(decision: String):void
		{
			var nextTask: String;
			
			if(webService)
				
				nextTask = TuGraz.nextTask;
			
			else
				
				nextTask = InnerClass.nextTask;
			
			if(((decision == Strings.LEFT) && (GameInfo.leftAns == false)) || ((decision == Strings.RIGHT) && (GameInfo.rightAns == false)))
			{
				if(decision == Strings.LEFT)
				{
					GameInfo.leftAns = true;
					GameInfo.right = false;
					GameInfo.lTask = nextTask;
					lScreen = GameInfo.screen;
				}
					
				else
				{
					GameInfo.rightAns = true;
					GameInfo.right = true;
					GameInfo.rTask = nextTask;
					rScreen = GameInfo.screen;
				}
				
				switch(GameInfo.screen)
				{
					case Strings.MOUNTAINS:
					{
						screenMountains = new Mountains();
						screenMountains.visible = true;
						this.addChild(screenMountains);
						insertExitBtn(680, 10);
						
						break;
					}
						
					case Strings.LAKE:
					{
						screenLake = new Lake();
						screenLake.visible = true;
						this.addChild(screenLake);
						insertExitBtn(680, 500);
						
						break;
					}
						
					case Strings.RIVER:
					{
						screenRiver = new River();
						screenRiver.visible = true;
						this.addChild(screenRiver);
						insertExitBtn(680, 10);
						
						break;
					}
				}
				
				if(nextTask == null)
				{
					_closed = true;
					screenClosed = new Closed(decision);
					screenClosed.visible = true;
					this.addChild(screenClosed);
				}
			}
				
			else 
			{
				if(decision == Strings.LEFT)
				{	
					GameInfo.right = false;
					screenSolved = new Solved();
					screenSolved.visible = true;
					this.addChild(screenSolved);			
				}
					
				else
				{	
					GameInfo.right = true;
					screenSolved = new Solved();
					screenSolved.visible = true;
					this.addChild(screenSolved);
				}
			}
		}
	}
}