package
{
	import flash.text.TextField;
	
	import objects.Colors;
	import objects.Strings;
	
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.display.Quad;
	
	
	public class HTML extends Sprite
	{
		private var textField: TextField = new TextField;
		private var href: String;
		private var text: String;
		private var rect: Quad;
		
		
		public function HTML(page: String, x: int, y: int)
		{
			super();
			
			var width: int;
			var height: int;
			
			if(page == Strings.ARTWORK_CREDITS)
			{
				href = "artwork_credits.html";
				text = "ARTWORK CREDITS";
				width = 150;
				height = 150;
			}
				
			else
			{
				href = "help.html";
				text = "HELP";
				textField.backgroundColor = 0x000000;
				textField.background = true;
				width = 35;
				height = 20;
			}
			
			textField.htmlText = "<a href = '" + href + "'><h1><u>" + text + "</u></h1></a>";
			textField.textColor = Colors.PINK_PEN;
			textField.x = x;
			textField.y = y;
			textField.width = width;
			textField.height = height;
			Starling.current.nativeOverlay.addChild(textField);
		}	
		
		public function removeHtml():void
		{
			Starling.current.nativeOverlay.removeChild(textField);
		}
	}
}