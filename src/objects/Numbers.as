package objects
{
	import starling.display.Sprite;
	
	public class Numbers extends Sprite
	{
		public function Numbers()
		{
			super();
		}
		
		public static function randomNumber(min: int, max: int): Number
		{
			return Number(Math.floor(Math.random() * (1 + max - min) + min));
		}
		
		public static function roundDecimal(num: Number, precision:int): Number
		{
			var decimal: Number = Math.pow(10, precision);
			
			return Math.round(decimal * num) / decimal;
		}
	}
}