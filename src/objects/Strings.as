package objects
{
	import starling.display.Sprite;
	
	public class Strings extends Sprite
	{
		// ANSWERS
		public static const NO: String = "no";
		public static const YES: String = "yes"; 
		
		// DISPATCH EVENTS
		
		public static const ABOUT: String = "about";
		public static const BACK: String = "back";
		public static const BACK_CORRECTION: String = "backCorrection";
		public static const BACK_CROSSROADS: String = "backCrossroads";
		public static const BACK_HOME: String = "backHome";
		public static const BACK_PROMPT: String = "backPrompt";
		public static const CLOSED: String = "closed";
		public static const CONTINUE: String = "continue";
		public static const CONT_PLAY: String = "contPlay";
		public static const CROSSROADS: String = "crossroads";
		public static const ERROR: String = "error";
		public static const FAILED: String = "failed";
		public static const GO_LAKE: String = "goLake";
		public static const GO_MOUNTAINS: String = "goMountains";
		public static const GO_RIVER: String = "goRiver";
		public static const HELP: String = "help";
		public static const LEFT: String = "left";
		public static const LETSGO: String = "letsgo";
		public static const PLAY: String = "play";
		public static const PLAY_AGAIN: String = "playAgain";
		public static const PROMPT: String = "prompt";
		public static const RIGHT: String = "right";
		public static const SECOND_CHOICE: String = "secondChoice";
		public static const SKIP: String = "skip";
		public static const SUBMIT: String = "submit";
		public static const TASK: String = "task";
		
		// GAME MODES
		
		public static const TU_GRAZ: String = "tuGraz";
		public static const INNER_CLASS: String = "innerClass";
		
		// HREF
		
		public static const ARTWORK_CREDITS: String = "artworkCredits";
		public static const HELP_REF: String = "help";
		
		// PEOPLE
		
		public static const FAIL_MISTAKES: String = "One or more of your answers were not correct. You will have to turn back and try again. Click\n\nto see the solution to the problems.";
		public static const FAIL_NO_MISTAKES: String = " You answers were correct, but unfortunately you were not lucky enough and you have not managed to advance through the path you chose. You will have to turn back and try again.";
		public static const SUB_NO_MISTAKES: String = "Your answers were correct, and you have managed to advance through the way you have chosen!";
		
		
		// SCREENS
		
		public static const LAKE: String = "lake";
		public static const MOUNTAINS: String = "mountains";
		public static const RIVER: String = "river";
		
		// SIMBLES
		
		public static const ANS: String = "ans";
		public static const C_BRACKET: String = ")";
		public static const DEL: String = "del";
		public static const DIVIDE: String = "/";
		public static const MINUS: String = "-";
		public static const O_BRACKET: String = "(";
		public static const PLUS: String = "+";
		public static const TIMES: String = "*";
		
		// TASKS TYPES
		
		public static const EV: String = "ev";
		public static const CRO: String = "cro";
		public static const YN: String = "yn";
		
		// TUGRAZ COMMANDS
		
		public static const COMMIT: String = "commit";
		public static const COMPETENCES: String = "competences";
		public static const DELETE: String = "delete";
		public static const INIT: String = "init";
		public static const NEXT_TASK: String = "nextTask";
		public static const PROC: String = "processes";
		public static const REPORT: String = "report";
		public static const STOP: String = "stop";
		
		public static const DELETE_TOT: String = "deleteEverything";
		
		//TUGRAZ METHODS
		
		public static const GET: String = "get";
		public static const POST: String = "post";
		
		// TUGRAZ PATHS
		
		public static const BASE_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/";
		public static const COMMIT_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/commitresponse";
		public static const COMPETENCES_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/getcompetences?learningprocessid=";
		public static const DELETE_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/deletelearningprocess?learningprocessid=";
		public static const INIT_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/initlearningprocess";
		public static const NEXT_TASK_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/getnextproblem?learningprocessid=";
		public static const PROC_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/getlearningprocesses?clientid=gala";
		public static const REPORT_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/";
		public static const STOP_PATH: String = "http://css-kmi.tugraz.at:8080/compod/rest/stoplearningprocess?learningprocessid=";
		
		
		public function Strings()
		{
			super();
		}
	}
}