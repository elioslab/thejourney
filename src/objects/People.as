package objects
{
	import starling.core.Starling;
	import starling.display.Sprite;
	import starling.display.MovieClip;
	import starling.events.Event;
	
	
	public class People extends Sprite
	{
		private var personArt1: MovieClip;
		private var personArt2: MovieClip;
		private var personArt3: MovieClip;
		
		public function People()
		{
			super();
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage():void
		{
			this.removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
			createPeopleArt();
		}
		
		private function createPeopleArt():void
		{
			personArt1 = new MovieClip(Assets.getAtlas().getTextures("personaggio1_"), 10);
			personArt1.x = Math.ceil(400);
			personArt1.y = Math.ceil(- personArt1.height / 2);
			Starling.juggler.add(personArt1);
			this.addChild(personArt1);
			
			personArt2 = new MovieClip(Assets.getAtlas().getTextures("personaggio2_"), 10);
			personArt2.x = Math.ceil(200);
			personArt2.y = Math.ceil(- personArt2.height / 2);
			Starling.juggler.add(personArt2);
			this.addChild(personArt2);
			
			personArt3 = new MovieClip(Assets.getAtlas().getTextures("personaggio3_"), 10);
			personArt3.x = Math.ceil(0);
			personArt3.y = Math.ceil(- personArt3.height / 2);
			Starling.juggler.add(personArt3);
			this.addChild(personArt3);
		}
	}
}