package objects
{
	import starling.display.Sprite;
	
	public class Colors extends Sprite
	{
		public static const BLUE: uint = 0x16699B;
		public static const PEN: uint = 0x003399;
		public static const PINK: uint = 0xC81C72;
		public static const PINK_PEN: uint = 0xEB1168;
		public static const VIOLET: uint = 0x995099;
		public static const RED: uint = 0xff0000;
		public static const YELLOW: uint = 0xCC752E;
		
		
		public function Colors()
		{
			super();
		}
	}
}