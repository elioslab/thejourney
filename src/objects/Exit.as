package objects
{
	import flash.desktop.NativeApplication;
	import flash.events.Event;
	import flash.system.fscommand;
	
	import objects.Colors;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Quad;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.text.TextField;
	import inner_class.InnerClass;


	public class Exit extends Sprite
	{
		private var window: Quad;
		private var bubble: Image;
		private var yes: Button;
		private var no: Button;
		private var boy: Image;
		private var girl: Image;
		private var msg: TextField;
		
		
		public function Exit()
		{
			super();
			
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		private function onAddedToStage(): void
		{
			window = new Quad(310, 200, 0x85ADFF, true);
			window.x = 235;
			window.y = 200;
			this.addChild(window);
			yes = new Button(Assets.getTexture("yes"));
			yes.width = yes.height = 80;
			yes.x = 300;
			yes.y = 250;
			this.addChild(yes);
			no = new Button(Assets.getTexture("no"));
			no.width = yes.width;
			no.height = yes.height;
			no.x = yes.x + 95;
			no.y = yes.y;
			this.addChild(no);
			bubble = new Image(Assets.getTexture("bubble2"));
			bubble.width = 250;
			bubble.height = 150;
			bubble.x = 245;
			bubble.y = 100;
			this.addChild(bubble);
			msg = new TextField(190, 100, "Do you really want to exit?", "HoboStd", 20, Colors.PEN);
			msg.x = 275;
			msg.y = 115;
			//msg.border = true;
			this.addChild(msg);
			boy = new Image(Assets.getTexture("boy2"));
			boy.width = 132;
			boy.height = 300;
			boy.x = 480;
			boy.y = 101;
			this.addChild(boy);
			girl = new Image(Assets.getTexture("girl"));
			girl.width = 132;
			girl.height = 300;
			girl.x = 165;
			girl.y = 102;
			this.addChild(girl);
			
			this.addEventListener(starling.events.Event.TRIGGERED, onClick);
		}
		
		private function onClick(event: starling.events.Event): void
		{
			var buttonClicked: Button = event.target as Button;
			
			if((buttonClicked as Button) == no)
				
				this.removeFromParent();
			
			else if((buttonClicked as Button) == yes)
			{	
				printStatistic();
				NativeApplication.nativeApplication.exit();
			}
		}
		
		private function printStatistic(): void
		{
			trace("\n\nSTATISTICS\n\n");
			//InnerClass.traceValues();
			//InnerClass.traceTasksN();
		}
	}
}