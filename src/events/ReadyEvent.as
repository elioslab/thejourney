package events
{
	import starling.events.Event;
	
	public class ReadyEvent extends Event
	{
		public static const DB_CONNECTED: String = "dbConnected";
		public static const DB_RESPONSE: String = "dbResponse";
		public static const ROW_INSERTED: String = "rowInserted";
		public static const SCREEN_ADDED: String = "screenAdded";
		public static const SVC_COMMITTED: String = "svcCommitted";
		public static const SVC_COMP: String = "svcComp";
		public static const SVC_DELETE: String = "svcDelete";
		public static const SVC_INIT: String = "svcInit";
		public static const SVC_NEXT1: String = "svcNext";
		public static const SVC_NEXT2: String = "svcNext";
		public static const SVC_STOP: String = "svcstop";
		public static const TABLE_CREATED: String = "tableCreated";
		public static const TABLE_FILLED: String = "tableFilled";
		public static const TASK_SETTED: String = "taskSetted";
		
		public var params: Object;
		
		
		public function ReadyEvent(type:String, _params: Object = null, bubbles:Boolean=false, data:Object=null)
		{
			super(type, bubbles, data);
			this.params = _params;
		}
	}
}