package inner_class
{
	import starling.display.Sprite;
	
	public class InnerClassTask extends Sprite
	{
		private var _taskId: String;					// Id del task.
		private var _compId: String;					// Id della competenza a cui appartiene il task.
		private var _compInvolved: Array;				// Competenze coinvolte nella verifica.
		private var _upgrade: Array;					// Valori di upgrade dei task.
		private var _downgrade: Array;					// Valori di downgrade dei task.
		private var _difficulty: int;
		
		
		public var string: String;
		
		public function InnerClassTask(task: String, diff: int, comp: String, params: Array)
		{
			super();
			
			_taskId = task;
			_difficulty = diff;
			_compId = comp;
			
			var i: int;
			var n: int;
			var l: int = params.length;
			
			_compInvolved = new Array();
			_upgrade = new Array();
			_downgrade = new Array();
			
			for(i = 0, n = 0; i < l;  n++)
			{
				_compInvolved[n] = params[i++];
				string = "\n " + _compInvolved[n].toString(); 
				_upgrade[n] = params[i++];
				string += " " + _upgrade[n].toString() + " "; 
				_downgrade[n] = params[i++];
				string += _downgrade[n] + "\n";
			}
		}
		
		public function getString(): String
		{
			var str: String = " " + taskId + " " + difficulty + " " + _compId + "\nTask involved = ";
			
			for(var i: int = 0; i < _compInvolved.length; i++)
			{
				str += _compInvolved[i] + " " + _upgrade[i] + " -" + _downgrade + "\n"; 
			}
	
			return str;
		}
		
		public function get difficulty():int
		{
			return _difficulty;
		}
		
		public function get downgrade():Array
		{
			return _downgrade;
		}
		
		public function get upgrade():Array
		{
			return _upgrade;
		}
		
		public function get compInvolved():Array
		{
			return _compInvolved;
		}
		
		public function get compId():String
		{
			return _compId;
		}
		
		public function get taskId():String
		{
			return _taskId;
		}		
	}
}
