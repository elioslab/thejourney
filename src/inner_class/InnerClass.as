package inner_class
{
	import objects.Numbers;
	import objects.Strings;
	
	import starling.display.Sprite;
	
	public class InnerClass extends Sprite
	{
		private static var _nAquiredComp: int;
		private static var aquiredComp: Array;
		private static var _nextTask: String;
		public static var theTask: InnerClassTask;
		private static var comp1: InnerClassComp;
		private static var comp2: InnerClassComp;
		private static var comp3: InnerClassComp;
		private static var comp4: InnerClassComp;
		private static var comp5: InnerClassComp;
		public static var nTasks: int;
		
		
		public static var v : String;
		
		
		public function InnerClass()
		{
			super();
			
			_nAquiredComp = 1;
			aquiredComp = new Array(1, 0, 0, 0, 0);
			nTasks = 0;
			
			setGame();
		}
		
		public static function get nAquiredComp():int
		{
			return _nAquiredComp;
		}
		
		public static function get nextTask(): String
		{
			return _nextTask;
		}
		
		private function setGame(): void
		{
			comp1 = new InnerClassComp("comp1", 0.9);
			comp2 = new InnerClassComp("comp2", 0.5);
			comp3 = new InnerClassComp("comp3", 0.5);
			comp4 = new InnerClassComp("comp4", 0.3);
			comp5 = new InnerClassComp("comp5", 0.3);
		}
		
		public static function loadNextTask(): void
		{
			var random: int;
			
			traceValues();			
			
			if(comp1.p < 0.9)
			{
				random = Numbers.randomNumber(1, 5);
				
				switch(random)
				{
					case 1:
					{
						_nextTask = "task01";
						theTask = comp1.task1;
						
						break;
					}
						
					case 2:
					{
						_nextTask = "task02";
						theTask = comp1.task1;
						
						break;
					}
						
					case 3:
					{
						_nextTask = "task03";
						theTask = comp1.task3;
						
						break;
					}
						
					case 4:
					{
						_nextTask = "task04";
						theTask = comp1.task4;
						
						break;
					}
						
					case 5:
					{
						_nextTask = "task05";
						theTask = comp1.task5;
						
						break;
					}
						
					default:
					{
						trace("\nTASK NON SETTATO!!!! 109\n");
					}
				}
			}
				
			else if(comp2.p < 0.9 || comp3.p < 0.9 || comp4.p < 0.9)
			{
				// Se sono qui, almeno una fra le competenze 2, 3 e 4 non è stata acquisita.
				
				random = setComp();
				var random2: int = Numbers.randomNumber(1, 5);
				
				switch(random)
				{
					case 1:
					{
						switch(random2)
						{
							case 1:
							{
								_nextTask = "task06";
								theTask = comp2.task1;
								
								break;
							}
								
							case 2:
							{
								_nextTask = "task07";
								theTask = comp2.task2;
								
								break;
							}
								
							case 3:
							{
								_nextTask = "task08";
								theTask = comp2.task3;
								
								break;
							}
								
							case 4:
							{
								_nextTask = "task09";
								theTask = comp2.task4;
								
								break;
							}
								
							case 5:
							{
								_nextTask = "task10";
								theTask = comp2.task5;
								
								break;
							}
								
							default:
							{
								trace("\nTASK NON SETTATO!!!! 170\n");
							}
						}
						
						break;
					}
						
					case 2:
					{
						switch(random2)
						{
							case 1:
							{
								_nextTask = "task11";
								theTask = comp3.task1;
								
								break;
							}
								
							case 2:
							{
								_nextTask = "task12";
								theTask = comp3.task2;
								
								break;
							}
								
							case 3:
							{
								_nextTask = "task13";
								theTask = comp3.task3;
								
								break;
							}
								
							case 4:
							{
								_nextTask = "task14";
								theTask = comp3.task4;
								
								break;
							}
								
							case 5:
							{
								_nextTask = "task15";
								theTask = comp3.task5;
								
								break;
							}
								
							default:
							{
								trace("\nTASK NON SETTATO!!!! 223\n");
							}
						}
						
						break;
					}
						
					case 3:
					{
						switch(random2)
						{
							case 1:
							{
								_nextTask = "task16";
								theTask = comp4.task1;
								
								break;
							}
								
							case 2:
							{
								_nextTask = "task17";
								theTask = comp4.task2;
								
								break;
							}
								
							case 3:
							{
								_nextTask = "task18";
								theTask = comp4.task3;
								
								break;
							}
								
							case 4:
							{
								_nextTask = "task19";
								theTask = comp4.task4;
								
								break;
							}
								
							case 5:
							{
								_nextTask = "task20";
								theTask = comp4.task5;
								
								break;
							}
								
							default:
							{
								trace("\nTASK NON SETTATO!!!! 276\n");
							}
						}
						
						break;
					}
				}
			}
				
			else if(comp5.p < 0.9)
			{
				// Se sono qui è perchè le competenze 1, 2, 3 e 4 sono state acquisite.
				
				random = Numbers.randomNumber(1, 5);
				
				switch(random)
				{
					case 1:
					{
						_nextTask = "task21";
						theTask = comp5.task1;
						
						break;
					}
						
					case 2:
					{
						_nextTask = "task22";
						theTask = comp5.task2;
						
						break;
					}
						
					case 3:
					{
						_nextTask = "task23";
						theTask = comp5.task3;
						
						break;
					}
						
					case 4:
					{
						_nextTask = "task24";
						theTask = comp5.task4;
						
						break;
					}
						
					case 5:
					{
						_nextTask = "task25";
						theTask = comp5.task5;
						
						break;
					}
						
					default:
					{
						trace("\nTASK NON SETTATO!!!! 335\n");
					}
				}
			}
			
			else
			{
				_nextTask = null;
				theTask = null;
				trace("\nTASK NON SETTATO!!!!\n");
			}
			
			if(_nextTask != null)
			{
				InnerClass.setScreen();
				setTheTask();
				trace("next task = " + _nextTask);
			}
		}
		
		public static function traceValues(): void
		{
			trace("\n P Values:\tp1 = " + comp1.p + " p2 = " + comp2.p +" p3 = " + comp3.p + " p4 = " + comp4.p + " p5 = " + comp5.p);
		}
		
		private static function setComp(): int
		{
			var max: int;
			
			if(comp5.p < 0.9)
				
				max = 4 - _nAquiredComp;
			
			else
				
				max = 5 -_nAquiredComp; 
			
			var random: int = Numbers.randomNumber(1, max);
			trace("ACQUIRED " + _nAquiredComp);
			var count: int = 0;
			
			for(var i: int = 1; i < 5; i++)
			{
				if(aquiredComp[i] == 0)
				{
					count++;
				}
				
				if(count == random)
					
					break;
			}
			
			trace("\n COMPETENZA = " + (i + 1) + "\n");
			
			return i;
		}
		
		private static function setTheTask():void
		{
			switch(_nextTask)
			{
				case comp1.task1.taskId:
				{
					theTask = comp1.task1;
					
					break;
				}
					
				case comp1.task2.taskId:
				{
					theTask = comp1.task2;
					
					break;
				}
					
				case comp1.task3.taskId:
				{
					theTask = comp1.task3;
					
					break;
				}
					
				case comp1.task4.taskId:
				{
					theTask = comp1.task4;
					
					break;
				}
					
				case comp1.task5.taskId:
				{
					theTask = comp1.task5;
					
					break;
				}
					
				case comp2.task1.taskId:
				{
					theTask = comp2.task1;
					
					break;
				}
					
				case comp2.task2.taskId:
				{
					theTask = comp2.task2;
					
					break;
				}
					
				case comp2.task3.taskId:
				{
					theTask = comp2.task3;
					
					break;
				}
					
				case comp2.task4.taskId:
				{
					theTask = comp2.task4;
					
					break;
				}
					
				case comp2.task5.taskId:
				{
					theTask = comp2.task5;
					
					break;
				}
					
				case comp3.task1.taskId:
				{
					theTask = comp3.task1;
					
					break;
				}
					
				case comp3.task2.taskId:
				{
					theTask = comp3.task2;
					
					break;
				}
					
				case comp3.task3.taskId:
				{
					theTask = comp3.task3;
					
					break;
				}
					
				case comp3.task4.taskId:
				{
					theTask = comp4.task1;
					
					break;
				}
					
				case comp3.task5.taskId:
				{
					theTask = comp3.task5;
					
					break;
				}
					
				case comp4.task1.taskId:
				{
					theTask = comp4.task1;
					
					break;
				}
					
				case comp4.task2.taskId:
				{
					theTask = comp4.task2;
					
					break;
				}
					
				case comp4.task3.taskId:
				{
					theTask = comp4.task3;
					
					break;
				}
					
				case comp4.task4.taskId:
				{
					theTask = comp4.task4;
					
					break;
				}
					
				case comp4.task5.taskId:
				{
					theTask = comp4.task5;
					
					break;
				}
					
				case comp5.task1.taskId:
				{
					theTask = comp5.task1;
					
					break;
				}
					
				case comp5.task2.taskId:
				{
					theTask = comp5.task2;
					
					break;
				}
					
				case comp5.task3.taskId:
				{
					theTask = comp5.task3;
					
					break;
				}
					
				case comp5.task4.taskId:
				{
					theTask = comp5.task4;
					
					break;
				}
					
				case comp5.task5.taskId:
				{
					theTask = comp5.task5;
					
					break;
				}
			}
		}
		
		private static function loadComp(): void
		{
			_nAquiredComp = 0;
			
			if(comp1.p < 0.9)
			{
				aquiredComp[0] = 0;
			}
				
			else 
			{
				aquiredComp[0] = 1;
				_nAquiredComp++;
			}
			
			if(comp2.p < 0.9)
			{
				aquiredComp[1] = 0;
			}
				
			else 
			{
				aquiredComp[1] = 1;
				_nAquiredComp++;
			}
			
			if(comp3.p < 0.9)
			{
				aquiredComp[2] = 0;
			}
				
			else 
			{
				aquiredComp[2] = 1;
				_nAquiredComp++;
			}
			
			if(comp4.p < 0.9)
			{
				aquiredComp[3] = 0;
			}
				
			else 
			{
				aquiredComp[3] = 1;
				_nAquiredComp++;
			}
			
			if(comp5.p < 0.9)
			{
				aquiredComp[4] = 0;
			}
				
			else 
			{
				aquiredComp[4] = 1;
				_nAquiredComp++;
			}
		}
		
		private static function setScreen(): void
		{			
			switch(_nextTask)
			{
				case "task06": case "task07": case "task08": case "task10": case "task11": case "task12": case "task21": case "task22":
				case "task23": case "task24":
				{
					GameInfo.screen = Strings.MOUNTAINS;
					
					break;
				}
					
				case "task09": case "task13": case "task14": case "task16": case "task18": case "task19": case "task25":
				{
					GameInfo.screen = Strings.LAKE;
					
					break;
				}
					
				case "task01": case "task02": case "task03": case "task04": case "task05": case "task15": case "task17": case "task20":
				{
					GameInfo.screen = Strings.RIVER;
					
					break;
				}
			}
		}
		
		public static function commit(): void
		{
			var i: int;
			var l: int = theTask.compInvolved.length;
			var ud: Number;
			var inc: Boolean;
			
			nTasks++;
			
			trace("BEFORE");
			traceValues();
			
			for(i = 0; i < l; i++)
			{
				if(GameInfo.commit)
				{
					ud = theTask.upgrade[i];
					inc = true;
				}
				
				else
				{	
					ud = - theTask.downgrade[i];
					inc = false;
				}
				
				trace(ud + " " + theTask.upgrade[i]);
				
				switch(theTask.compInvolved[i])
				{
					case "comp1":
					{
						if(comp1.p + ud < 0.0)
							
							comp1.p = 0.0;
						
						else if(comp1.p + ud >= 1.0)
							
							comp1.p = 1.0;
							
						else
							
							comp1.p = Math.round((comp1.p + ud) * 10) / 10;
						
						if(i == 0 && inc)
							
							comp1.nCorrTask++;
						
						if(i == 0)
							
							comp1.nTask++;
						
						break;
					}
						
					case "comp2":
					{
						if(comp2.p + ud < 0.0)
							
							comp2.p = 0.0;
							
						else if(comp2.p + ud >= 1.0)
							
							comp2.p = 1.0;
							
						else
							
							comp2.p = Math.round((comp2.p + ud) * 10) / 10;
						
						if(i == 0 && inc)
							
							comp2.nCorrTask++;
						
						break;
						
						if(i == 0)
							
							comp2.nTask++;
					}
						
					case "comp3":
					{
						if(comp3.p + ud < 0.0)
							
							comp3.p = 0.0;
							
						else if(comp3.p + ud >= 1.0)
							
							comp3.p = 1.0;
							
						else
							
							comp3.p = Math.round((comp3.p + ud) * 10) / 10;
						
						if(i == 0 && inc)
							
							comp3.nCorrTask++;
						
						if(i == 0)
							
							comp3.nTask++;
						
						break;
					}
						
					case "comp4":
					{
						if(comp4.p + ud < 0.0)
							
							comp4.p = 0.0;
							
						else if(comp4.p + ud >= 1.0)
							
							comp4.p = 1.0;
							
						else
							
							comp4.p = Math.round((comp4.p + ud) * 10) / 10;
						
						if(i == 0 && inc)
							
							comp4.nCorrTask++;
						
						if(i == 0)
							
							comp4.nTask++;
						
						break;
					}
						
					case "comp5":
					{
						if(comp5.p + ud < 0.0)
							
							comp5.p = 0.0;
							
						else if(comp5.p + ud >= 1.0)
							
							comp5.p = 1.0;
							
						else
							
							comp5.p = Math.round((comp5.p + ud) * 10) / 10;
						
						if(i == 0 && inc)
							
							comp5.nCorrTask++;
						
						if(i == 1)
							
							comp5.nTask++;
						
						break;
					}
				}
				
				loadComp();
				
				trace("AFTER");
				traceValues();
			}
		}

		public static function traceTasksN(): void
		{
			trace("\nThe player answered to " + nTasks + " tasks\n");
			trace("\n\n\t\tCOMPETENCE\tComp1\tComp2\tComp3\tComp4\tComp5");
			trace("\n\t\tTRIED\t\t" + comp1.nTask + "\t" + comp2.nTask + "\t" + comp3.nTask + "\t" + comp4.nTask + "\t" + comp5.nTask);
			trace("\n\t\tCORRECT\t\t" + comp1.nCorrTask + "\t" + comp2.nCorrTask + "\t" + comp3.nCorrTask + "\t" + comp4.nCorrTask + "\t" + comp5.nCorrTask + "\n\n");
		}
	}
}
