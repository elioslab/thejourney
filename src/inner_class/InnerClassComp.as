package inner_class
{
	import starling.display.Sprite;
	
	public class InnerClassComp extends Sprite
	{
		private var _compId: String;
		public var p: Number;
		private var _task1: InnerClassTask;
		private var _task2: InnerClassTask;
		private var _task3: InnerClassTask;
		private var _task4: InnerClassTask;
		private var _task5: InnerClassTask;
		public var nTask: int;
		public var nCorrTask: int;
				
		
		public function InnerClassComp(comp: String, prob: Number)
		{
			super();
			
			var params: Array;
			
			_compId = comp;
			p = prob;
			nTask = 0;
			nCorrTask = 0;
			
			switch(_compId)
			{
				case "comp1":
				{
					params = new Array(_compId, "0.2", "0.2");
					_task1 = new InnerClassTask("task01", 1, _compId, params)
					_task2 = new InnerClassTask("task02", 1, _compId, params)
					_task3 = new InnerClassTask("task03", 1, _compId, params)
					_task4 = new InnerClassTask("task04", 2, _compId, params)
					_task5 = new InnerClassTask("task05", 2, _compId, params)
					
					break;
				}
					
				case "comp2":
				{
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.1", "0.1");
					_task1 = new InnerClassTask("task06", 1, _compId, params)
					params = new Array(_compId, "0.2", "0.2");
					_task2 = new InnerClassTask("task07", 1, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.2", "0.1");
					_task3 = new InnerClassTask("task08", 2, _compId, params)
					params = new Array(_compId, "0.2", "0.2");
					_task4 = new InnerClassTask("task09", 2,_compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.2", "0.1");
					_task5 = new InnerClassTask("task10", 3, _compId, params)
					
					break;
				}
					
				case "comp3":
				{
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.1", "0.1");
					_task1 = new InnerClassTask("task11", 2, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.1", "0.1");
					_task2 = new InnerClassTask("task12", 2, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.2", "0.1");
					_task3 = new InnerClassTask("task13", 2, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.1", "0.1");
					_task4 = new InnerClassTask("task14", 3, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.2", "0.1");
					_task5 = new InnerClassTask("task15", 3, _compId, params)
					
					break;
				}
					
				case "comp4":
				{
					params = new Array(_compId, "0.2", "0.2");
					_task1 = new InnerClassTask("task16", 1, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.1", "0.1");
					_task2 = new InnerClassTask("task17", 2, _compId, params)
					params = new Array(_compId, "0.2", "0.2");
					_task3 = new InnerClassTask("task18", 2, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.1", "0.1");
					_task4 = new InnerClassTask("task19", 3, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp2", "0.1", "0.1", "comp1", "0.1", "0.1");
					_task5 = new InnerClassTask("task20", 3, _compId, params)
					
					break;
				}
					
				case "comp5":
				{
					params = new Array(_compId, "0.2", "0.2");
					_task1 = new InnerClassTask("task21", 1, _compId, params)
					params = new Array(_compId, "0.2", "0.2");
					_task2 = new InnerClassTask("task22", 2, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.1", "0.1");
					_task3 = new InnerClassTask("task23", 2, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp1", "0.1", "0.1");
					_task4 = new InnerClassTask("task24", 3, _compId, params)
					params = new Array(_compId, "0.2", "0.2", "comp4", "0.1", "0.1");
					_task5 = new InnerClassTask("task25", 3, _compId, params)
					
					break;
				}
			}
		}
		
		public function get task5():InnerClassTask
		{
			return _task5;
		}
		
		public function get task4():InnerClassTask
		{
			return _task4;
		}
		
		public function get task3():InnerClassTask
		{
			return _task3;
		}
		
		public function get task2():InnerClassTask
		{
			return _task2;
		}
		
		public function get task1():InnerClassTask
		{
			return _task1;
		}
		
		public function get compId():String
		{
			return _compId;
		}
		
		public function getString(): String
		{
			var str: String = " " + _compId + " " + p + "\n";
			
			return str;
		}
	}
}