package
{
	import flash.display.Bitmap;
	import flash.utils.Dictionary;
	
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;
	
	public class Assets
	{	
		[Embed(source = "../media/graphics/welcome/home.png")]
		public static const home: Class;
		
		[Embed(source = "../media/graphics/arrows/left.png")]
		public static const lArrow: Class;
		
		[Embed(source = "../media/graphics/arrows/right.png")]
		public static const rArrow: Class;
		
		[Embed(source = "../media/graphics/scenes/girl2.png")]
		public static const girl: Class;
		
		[Embed(source = "../media/graphics/scenes/exit.png")]
		public static const exit: Class;
		
		[Embed(source = "../media/graphics/scenes/girl.png")]
		public static const girl1: Class;
		
		[Embed(source = "../media/graphics/scenes/boy2.png")]
		public static const boy2: Class;
		
		[Embed(source = "../media/graphics/scenes/bubble1.png")]
		public static const bubble1: Class;
		
		[Embed(source = "../media/graphics/scenes/bubble2.png")]
		public static const bubble2: Class;
		
		[Embed(source = "../media/graphics/scenes/loading.png")]
		public static const loading: Class;
		
		[Embed(source = "../media/graphics/scenes/closed.png")]
		public static const closed: Class;
		
		[Embed(source = "../media/graphics/scenes/welcome.jpg")]
		public static const bgWelcome: Class;
		
		[Embed(source = "../media/graphics/scenes/end.jpg")]
		public static const bgEnd: Class;
		
		[Embed(source = "../media/graphics/scenes/lake.jpg")]
		public static const bgLake: Class;
		
		[Embed(source = "../media/graphics/scenes/howTo.jpg")]
		public static const howTo: Class;
		
		[Embed(source = "../media/graphics/scenes/river.png")]
		public static const bgRiver: Class;
	
		[Embed(source = "../media/graphics/scenes/mountains.jpg")]
		public static const bgMountains: Class;
		
		[Embed(source = "../media/graphics/walking/background/sky.jpg")]
		public static const walking_sky: Class;
		
		[Embed(source = "../media/graphics/walking/background/mountain.png")]
		public static const walking_mountains: Class;
		
		[Embed(source = "../media/graphics/walking/background/hills.png")]
		public static const walking_hills: Class;
		
		[Embed(source = "../media/graphics/walking/background/road.png")]
		public static const walking_road: Class;
		
		[Embed(source = "../media/graphics/scenes/flowers.png")]
		public static const walking_flowers: Class;
		
		[Embed(source = "../media/graphics/scenes/notes.png")]
		public static const bgNotes: Class;
		
		[Embed(source = "../media/graphics/scenes/crossroads.jpg")]
		public static const bgCrossroads: Class;
		
		[Embed(source = "../media/graphics/scenes/here.png")]
		public static const error: Class;
		
		[Embed(source = "../media/graphics/scenes/question.png")]
		public static const question: Class;
		
		[Embed(source = "../media/graphics/howto/continue.png")]
		public static const contButton: Class;
		
		[Embed(source = "../media/graphics/howto/skip.png")]
		public static const skipButton: Class;
		
		[Embed(source = "../media/graphics/howto/letsgo.png")]
		public static const letSGoButton: Class;
		
		[Embed(source = "../media/graphics/help/helpButton.png")]
		public static const helpButton: Class;
		
		[Embed(source = "../media/graphics/tasks/key.png")]
		public static const key: Class;
		
		[Embed(source = "../media/graphics/tasks/keyboard.png")]
		public static const keyboard: Class;
		
		[Embed(source = "../media/graphics/tasks/yes.png")]
		public static const yes: Class;
		
		[Embed(source = "../media/graphics/tasks/no.png")]
		public static const no: Class;
		
		[Embed(source = "../media/graphics/tasks/equal.png")]
		public static const equal: Class;
		
		[Embed(source = "../media/graphics/tasks/violet.png")]
		public static const violet: Class
		
		[Embed(source = "../media/graphics/tasks/blue.png")]
		public static const blue: Class
		
		[Embed(source = "../media/graphics/tasks/blue.png")]
		public static const decide: Class
		
		[Embed(source = "../media/graphics/about/name.png")]
		public static const name: Class;
		
		[Embed(source = "../media/graphics/about/university.png")]
		public static const university: Class;
		
		[Embed(source = "../media/graphics/about/hsharma.png")]
		public static const hsharma: Class;
		
		[Embed(source = "../media/graphics/about/starling.png")]
		public static const starling: Class;
		
		[Embed(source = "../media/graphics/about/unige.png")]
		public static const unige: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/image1.png")]
		public static const prompt1: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/image2.png")]
		public static const prompt2: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/image3.png")]
		public static const prompt3: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/image4.png")]
		public static const prompt4: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/image5.png")]
		public static const prompt5: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/image6.png")]
		public static const prompt6: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task11.png")]
		public static const task11: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task13.png")]
		public static const task13: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task14.png")]
		public static const task14: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task15_1.png")]
		public static const task15_1: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task15_2.png")]
		public static const task15_2: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task17_1.png")]
		public static const task17_1: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task17_2.png")]
		public static const task17_2: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task20_1.png")]
		public static const task20_1: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task20_2.png")]
		public static const task20_2: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task25_1.png")]
		public static const task25_1: Class;
		
		[Embed(source = "../media/graphics/prompt&revision/task25_2.png")]
		public static const task25_2: Class;
		
		[Embed(source = "../media/graphics/end/congratulations.png")]
		public static const congrats: Class;
		
		[Embed(source = "../media/graphics/end/youHaveReached.png")]
		public static const youHaveReached: Class;
		
		[Embed(source = "../media/graphics/end/theTopOfThe.png")]
		public static const theTopOfThe: Class;
		
		[Embed(source = "../media/graphics/end/highestMountain.png")]
		public static const highestMountain: Class;
		
		[Embed(source = "../media/fonts/HoboStd.otf", fontFamily = "HoboStd", embedAsCFF = true)]
		public static const font: Class;
		
		[Embed(source = "../media/graphics/mySpritesheet.png")]
		public static const atlasTextureGame: Class;
		
		[Embed(source = "../media/graphics/mySpritesheet.xml", mimeType="application/octet-stream")]
		public static const atlasXmlGame: Class;
		
		private static var gameTextures: Dictionary = new Dictionary();
		private static var gameTextureAtlas: TextureAtlas;
		private static var aboutTextureAtlas:TextureAtlas;
		
		
		public static function getTexture(name: String): Texture
		{
			if(gameTextures[name] == undefined)
			{
				var bitmap: Bitmap = new Assets[name]();
				gameTextures[name] = Texture.fromBitmap(bitmap);
			}
			
			return gameTextures[name];
		}
		
		public static function getAtlas(): TextureAtlas
		{
			if(gameTextureAtlas == null)
			{
				var texture: Texture = getTexture("atlasTextureGame");
				var xml: XML = XML(new atlasXmlGame());
				
				gameTextureAtlas = new TextureAtlas(texture, xml);
			}
			
			return gameTextureAtlas;
		}
	}
}