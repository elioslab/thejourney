package
{
	import objects.Numbers;
	import starling.display.Sprite;
	
	
	public class GameInfo extends Sprite
	{
		public static var leftAns: Boolean;			// Indica se l'utente ha risposto al task a sinistra. 
		public static var rightAns: Boolean;		// Indica se l'utente ha risposto al task a destra.
		public static var right: Boolean;			// Se true, all'incrocio ho scelto destra, sinistra altrimenti.
		public static var leftProb: Number;			// Risposta al task a sinistra se il task è di tipo choose right option oppure enter value.
		public static var leftYN: String;			// Risposta al task a sinistra se il task è di tipo yes / no.
		public static var rightProb: Number;		// Risposta al task a destra se il task è di tipo choose right option oppure enter value.
		public static var rightYN: String;			// Risposta al task a destra se il task è di tipo yes / no.
		public static var lCorrect: Boolean;		// Indica se l'utente ha risposto correttamente al task a sinistra.
		public static var rCorrect: Boolean;		// Indica se l'utente ha risposto correttamente al task a destra.
		public static var rAnswer: Number;			// Risposta corretta del task a destra in canso di task di tipo enter value o choose right option.
		public static var lAnswer: Number;			// Risposta corretta del task a sinistra in canso di task di tipo enter value o choose right option.
		public static var lType: String;			// Tipo del task a sinistra.
		public static var rType: String;			// Tipo del task a destra.
		private static var _lCost: int;
		private static var _rCost: int;
		public static var lTask: String;
		public static var rTask: String;
		public static var screen: String;			// Indica lo screen del task attualmente testato.
		public static var commit: Boolean;			// Indica se la risposta al task attuale è corretto.
		public static var lString: String;
		public static var rString: String;
		
		
		public function GameInfo()
		{
			super();
			
			ResetInfo();
		}
		
		public static function ResetInfo():void
		{
			leftAns = false;
			rightAns = false;
			leftProb = undefined;
			rightProb = undefined;
			commit = undefined;
		}
		
		public static function get rCost():int
		{
			return _rCost;
		}
		
		public static function setCost():void
		{
			if(right)
				
				_rCost = Numbers.randomNumber(10, 120);
				
			else
				
				_lCost = Numbers.randomNumber(10, 120);
		}
		
		public static function get lCost():int
		{
			return _lCost;
		}
	}
}