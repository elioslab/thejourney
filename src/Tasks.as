package
{
	import events.ReadyEvent;
	import objects.Numbers;
	import objects.Strings;
	import starling.display.Sprite;
	import inner_class.InnerClass;
	
	
	public class Tasks extends Sprite
	{
		private var _text: String;
		private var _type: String;
		private var returned: Array;
		private var _yNAnswer: String;			// Risposta corretta se il task è di tipo yes / no.
		private var _answer: Number;			// Risposta corretta se il task è di tipo choose right option oppure enter value.
		private var _option1: Number;			// Opzione 1 se il task è di tipo choose right option.
		private var _option2: Number;			// Opzione 2 se il task è di tipo choose right option.
		private var _yn: Boolean;				// Indica se _yNAnswer è un dato utile al task attuale.
		private var _cro: Boolean;				// Indica se _option1 e _option2 sono dati utili al task attuale.
		private static var _n1: Number;
		private static var _n2: Number;
		private static var _n3: Number;
		private static var _p1: Number;
		private static var _p2: Number;
		private static var _p3: Number;
		private static var _p4: Number;
		private var _taskSetted: Boolean;
		private var nextTask: String;
		
		
		public function Tasks()
		{
			super();
			
			_taskSetted = false;
			
			if(Game.webService)
				
				nextTask = TuGraz.nextTask;
			
			else
				
				nextTask = InnerClass.nextTask;
			
			Game.database.addEventListener(ReadyEvent.DB_RESPONSE, onResponse);
			Game.database.Query(nextTask);
		}
		
		public function get taskSetted():Boolean
		{
			return _taskSetted;
		}
		
		private function onResponse():void
		{
			loadTask();
		}
		
		public static function get n1():Number
		{
			return _n1;
		}
		
		public static function get n2():Number
		{
			return _n2;
		}
		
		public static function get n3():Number
		{
			return _n3;
		}
		
		public static function get p1():Number
		{
			return _p1;
		}
		
		public static function get p2():Number
		{
			return _p2;
		}
		
		public static function get p3():Number
		{
			return _p3;
		}
		
		public static function get p4():Number
		{
			return _p4;
		}
		
		public function get text(): String
		{
			return _text;
		}
		
		public function get ynAnswer(): String
		{
			return _yNAnswer;
		}
		
		public function get answer(): Number
		{
			return _answer;
		}
		
		public function get option1(): Number
		{
			return _option1;
		}
		
		public function get option2(): Number
		{
			return _option2;
		}
		
		public function get yn(): Boolean
		{
			return _yn;
		}
		
		public function get cro(): Boolean
		{
			return _cro;
		}
		
		private function loadTask(): void
		{
			_text = Database.params[0];
			
			switch(nextTask)
			{
				case "task01":
				{
					_n1 = Numbers.randomNumber(2, 1000);
					// n2 < n1
					_n2 = Numbers.randomNumber(1, n1);
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					
					if((n2 / n1) > 0.5)
						
						_yNAnswer = Strings.YES;
						
					else
						
						_yNAnswer = Strings.NO;
					
					_yn = true;
					_cro = false;
					
					break;
				}
					
				case "task02":
				{
					_n1 = Numbers.randomNumber(2, 1000);
					// n2 < n1
					_n2 = Numbers.randomNumber(1, n1);
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					
					_answer = n2 / n1;
					_option1 = Numbers.randomNumber(1, 100) / 100;
					_option2 = (n1 - n2) / n1;
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task03":
				{
					_n1 = Numbers.randomNumber(2, 1000);
					// n2 < n1
					_n2 = Numbers.randomNumber(1, n1);
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					
					_answer = n2 / n1;
					_option1 = n1 / n2;
					_option2 = Numbers.randomNumber(1, 100) / 100;
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task04":
				{	
					_n1 = Numbers.randomNumber(2, 1000);
					// n2 < n1
					_n2 = Numbers.randomNumber(1, n1);
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					
					_answer = (n1 - n2) / n1;
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task05":
				{
					_n1 = Numbers.randomNumber(2, 1000);
					// n2 < n1
					_n2 = Numbers.randomNumber(1, n1);
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					
					_answer = n2 / n1;
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task06":
				{
					_p1 = Numbers.randomNumber(1, 99) / 100;
					// p1 + p2 <= 1 --> p2 <= 1 - p1
					_p2 = Numbers.randomNumber(1, ((1 - p1) * 100)) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					
					if((1 - (p1 + p2)) > 0.5)
						
						_yNAnswer = Strings.YES;
						
					else
						
						_yNAnswer = Strings.NO;
					
					_yn = true;
					_cro = false;
					
					break;
				}
					
				case "task07":
				{
					_p1 = Numbers.randomNumber(1, 99) / 100;
					// p1 + p2 <= 1 --> p2 <= 1 - p1
					_p2 = Numbers.randomNumber(1, ((1 - p1) * 100)) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					
					_answer = p1 + p2;
					_option1 = Numbers.randomNumber(1, 100) / 100;
					_option2 = p1 * p2;
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task08":
				{
					_n1 = Numbers.randomNumber(1, 364);
					// ((n1 + n2) / 365) <= 1 --> ni + n2 <= 365 --> n2 <= 365 - n1
					_n2 = Numbers.randomNumber(1, (365 - n1));
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					
					_answer = 1 - ((n1 + n2) / 365);
					_option1 = (n1 + n2) / 365;
					_option2 = Numbers.randomNumber(1, 100) / 100;
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task09":
				{
					_p1 = Numbers.randomNumber(1, 99) / 100;
					// p1 + p2 <= 1 --> p2 <= 1 - p1
					_p2 = Numbers.randomNumber(1, ((1 - p1) * 100)) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					
					_answer = p1 + p2;
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task10":
				{
					_n3 = Numbers.randomNumber(1, 1000);
					// ((n1 + n2) / n3) <= 1 --> n1 + n2 <= n3
					_n1 = Numbers.randomNumber(1, n3); 
					// n1 + n2 <= n3 --> n2 <= n3 - n1
					_n2 = Numbers.randomNumber(1, (_n3 - _n1));
					_p1 = 1;
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					_text = _text.replace("n3", n3);
					_text = _text.replace("p1", p1);
					
					_answer = 1 - ((n1 + n2) / n3);
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task11":
				{
					_p1 = Numbers.randomNumber(1, 99) / 100;
					_p2 = Numbers.randomNumber(1, 99) / 100;
					// 1 - p1 - p2 + p3 >= 0 --> p3 >= p1 + p2 - 1
					// 1 - p1 - p2 + p3 <= 1 --> p3 <= p1 + p2
					// --> p1 + p2 - 1 <= p3 <= p1 + p2
					if(p1 + p2 >= 1)
						
						_p3 = Numbers.randomNumber((_p1 + _p2 - 1), 99) / 100;
						
					else
						
						_p3 = Numbers.randomNumber(1, ((p1 + p2) * 100)) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					
					if((1 - p1 - p2 + p3) > 0.5)
						
						_yNAnswer = Strings.YES;
						
					else
						
						_yNAnswer = Strings.NO;
					
					_yn = true;
					_cro = false;
					
					break;
				}
					
				case "task12":
				{
					_p1 = Numbers.randomNumber(1, 99) / 100;
					_p2 = Numbers.randomNumber(1, 99) / 100;
					// 1 - p1 - p2 + p3 >= 0 --> p3 >= p1 + p2 - 1
					// 1 - p1 - p2 + p3 <= 1 --> p3 <= p1 + p2
					// --> p1 + p2 - 1 <= p3 <= p1 + p2
					if(p1 + p2 >= 1)
						
						_p3 = Numbers.randomNumber((_p1 + _p2 - 1), 99) / 100;
						
					else
						
						_p3 = Numbers.randomNumber(1, ((p1 + p2) * 100)) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					
					_answer = 1 - (p1 + p2 - p3);
					_option1 = 1 - (p1 * p2 * p3);
					_option2 = p1 + p2 - p3;
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task13":
				{
					_n1 = Numbers.randomNumber(1, 30); 
					_n2 = Numbers.randomNumber(1, (31 - _n1));
					// 1 - (n1 + n2 - n3) / 31 <= 1 --> n3 - n1 - n2 <= 0 --> n3 <= n1 + n2
					// 1 - (n1 + n2 - n3) / 31  >= 0 --> 31 - n1 - n2 + n3 >= 0 --> n3 >= n1 + n2 - 31
					_n3 = Numbers.randomNumber(1, (n1 + n2));
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					_text = _text.replace("n3", n3);
					
					_answer = (31 - (n1 + n2 - n3)) / 31;
					_option1 = 1 - ((31 - n1 - n2 + n3) / 31);
					_option2 = 1 - ((31 - n1 + n2 + n3) / 31);
					
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task14":
				{
					_p1 = Numbers.randomNumber(1, 99) / 100;
					_p2 = Numbers.randomNumber(1, 99) / 100;
					// 1 - p1 - p2 + p3 >= 0 --> p3 >= p1 + p2 - 1
					// 1 - p1 - p2 + p3 <= 1 --> p3 <= p1 + p2
					// --> p1 + p2 - 1 <= p3 <= p1 + p2
					if(p1 + p2 >= 1)
						
						_p3 = Numbers.randomNumber((_p1 + _p2 - 1), 99) / 100;
						
					else
						
						_p3 = Numbers.randomNumber(1, ((p1 + p2) * 100)) / 100;
					
					_n1 = 1;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					
					_answer = 1 - (p1 + p2 - p3);
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task15":
				{
					_p1 = Numbers.randomNumber(1, 99) / 100;
					_p2 = Numbers.randomNumber(1, 99) / 100;
					
					if(p1 > p2)
						
						_n1 = Numbers.randomNumber(int((_p1 - _p2) * 365), int(Math.min(_p1, 1 - _p2) * 365));
						
					else
						
						_n1 = Numbers.randomNumber(1, int((_p1 + 1 - _p2) * 365));
					
					_n2 = 365;
					_n3 = 1;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					_text = _text.replace("n3", n3);
					
					_answer = p1 + 1 - p2 - (n1/ 365);
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task16":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100;
					_p2 = Numbers.randomNumber(1, 100) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					
					_answer = p1 * p2;
					_option1 = p1 + p2;
					_option2 = Numbers.randomNumber(0, 100) / 100;
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task17":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100;
					_p2 = Numbers.randomNumber(1, 100) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					
					_answer = (1 - p1) * (1 - p2);
					_option1 = p1 * p2;
					_option2 = 1 - (p1 * p2);
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task18":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100;
					_p2 = Numbers.randomNumber(1, 100) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					
					_answer = p1 * p2;
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task19":
				{
					_n1 = Numbers.randomNumber(1, 1000);
					_n2 = Numbers.randomNumber(1, 365);
					_n3 = Numbers.randomNumber(1, n2);
					_p1 = Numbers.randomNumber(1, (n3 * 100 / n2)) / 100;
					
					_text = _text.replace("n1", n1);
					_text = _text.replace("n2", n2);
					_text = _text.replace("n3", n3);
					_text = _text.replace("p1", p1);
					
					_answer = n3 * p1 / n2;
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task20":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100;
					_p2 = Numbers.randomNumber(1, 100) / 100;
					_p3 = Numbers.randomNumber(1, p2 * 100) / 100;
					_n1 = 1;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					_text = _text.replace("n1", n1);
					
					_answer = (1 - p1) * (p2 + p3);
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task21":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100; 
					_p2 = Numbers.randomNumber(1, 100) / 100;
					_p3 = Numbers.randomNumber(1, 100) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					
					_answer = 1 - (p1 * p3);
					_option1 = p2 * p3;
					_option2 = p1 * p3;
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task22":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100;
					_p2 = Numbers.randomNumber(1, 100) / 100;
					_p3 = Numbers.randomNumber(1, 100) / 100;
					_n1 = 1;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					_text = _text.replace("n1", n1);
					
					_answer = 1 - (p1 * p3);
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task23":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100;
					_p2 = Numbers.randomNumber(1, 100) / 100;
					_p3 = Numbers.randomNumber(1, 100) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					
					_answer = 1 - (p1 * p3);
					_option1 = 1 - (p2 * p3);
					_option2 = 1 - p1;
					
					_yn = false;
					_cro = true;
					
					break;
				}
					
				case "task24":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100;
					_p2 = Numbers.randomNumber(1, 100) / 100;
					_p3 = Numbers.randomNumber(1, 100) / 100;
					_n1 = 1;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					_text = _text.replace("n1", n1);
					
					_answer = 1 - (p1 * p3);
					
					_yn = false;
					_cro = false;
					
					break;
				}
					
				case "task25":
				{
					_p1 = Numbers.randomNumber(1, 100) / 100;
					_p2 = Numbers.randomNumber(1, 100) / 100;
					_p3 = Numbers.randomNumber(1, 100) / 100;
					_p4 = Numbers.randomNumber(1, 100) / 100;
					
					_text = _text.replace("p1", p1);
					_text = _text.replace("p2", p2);
					_text = _text.replace("p3", p3);
					_text = _text.replace("p4", p4);
					
					_answer = p1 * (p3 * p2);
					
					_yn = false;
					_cro = false;
					
					break;
				}
			}
			
			_taskSetted = true;
			Game.database.dispatchEvent(new ReadyEvent(ReadyEvent.TASK_SETTED, true));
		}
	}
}