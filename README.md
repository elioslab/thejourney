# The Journey

The Journey is a simple serious game to teach basic concepts of probability theory to high school and entry-level university students, developed at the University of Genoa as a prototype implementation of a service-based adaptive SG, employing the CbKST services to provide basic adaptation features. 